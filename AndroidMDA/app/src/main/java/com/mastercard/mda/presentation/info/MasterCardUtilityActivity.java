package com.mastercard.mda.presentation.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;

import com.api.aspire.common.constant.ErrCode;
import com.mastercard.mda.App;
import com.mastercard.mda.R;
import com.mastercard.mda.common.constant.AppConstant;
import com.mastercard.mda.common.constant.IntentConstant;
import com.mastercard.mda.datalayer.entity.GetClientCopyResult;
import com.mastercard.mda.datalayer.repository.B2CDataRepository;
import com.mastercard.mda.domain.usecases.GetMasterCardCopy;
import com.mastercard.mda.presentation.base.CommonActivity;
import com.mastercard.mda.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.JustifiedTextView;

/**
 * Created by ThuNguyen on 6/19/2017.
 */

public class MasterCardUtilityActivity extends CommonActivity implements MasterCardUtility.View {

    MasterCardUtilityPresenter presenter;
    DialogHelper dialogHelper;
    private boolean delayAction = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isSwipeBack = false;
        setContentView(R.layout.activity_mastercard_copy_utility);
        toolbar.setNavigationIcon(R.drawable.ic_close_2);
        // Get mastercard utility type
        AppConstant.MASTERCARD_COPY_UTILITY type = (AppConstant.MASTERCARD_COPY_UTILITY) getIntent().getSerializableExtra(IntentConstant.MASTERCARD_COPY_UTILITY);
        switch (type){
            case TermsOfUse:
                title.setText(R.string.menu_term_of_use);
                if(savedInstanceState == null){
                    // Track GA
                    App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.TERMS_OF_USE.getValue());
                }
                break;
            case Privacy:
                title.setText(R.string.menu_privacy_policy);
                if(savedInstanceState == null){
                    // Track GA
                    App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.PRIVACY_POLICY.getValue());
                }
                break;
            case About:
                title.setText(R.string.menu_about);
                if(savedInstanceState == null){
                    // Track GA
                    App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.ABOUT_THIS_APP.getValue());
                }
                break;
        }
        if(savedInstanceState == null) {
            MasterCardUtilityFragment masterCardUtilityFragment = MasterCardUtilityFragment
                    .newInstance(type);
            masterCardUtilityFragment.setJustifyTextViewListener(justifyTextViewListener);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , masterCardUtilityFragment
                            , MasterCardUtilityFragment.class.getSimpleName())
                    .commit();
        }
        dialogHelper = new DialogHelper(this);
        presenter = new MasterCardUtilityPresenter(new GetMasterCardCopy(new B2CDataRepository()));
        presenter.attach(this);
        presenter.getMasterCardCopy(type.getValue());

    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onGetMasterCardCopy(GetClientCopyResult result) {
        if (result!=null){
            ((MasterCardUtilityFragment)getSupportFragmentManager()
                    .findFragmentByTag(MasterCardUtilityFragment.class.getSimpleName()))
                    .renderContent(result.getText());
        }
    }

    @Override
    public void loadEmptyContent() {
        MasterCardUtilityFragment fragment = ((MasterCardUtilityFragment)getSupportFragmentManager()
                .findFragmentByTag(MasterCardUtilityFragment.class.getSimpleName()));
        if(fragment == null) delayAction = true;
        else fragment.renderContent("");
    }

    void onFragmentCreated() {
        MasterCardUtilityFragment fragment = ((MasterCardUtilityFragment)getSupportFragmentManager()
                .findFragmentByTag(MasterCardUtilityFragment.class.getSimpleName()));
        if(delayAction) fragment.renderContent("");
    }

    @Override
    public void showErrorMessage(ErrCode errCode) {
        if(errCode == ErrCode.CONNECTIVITY_PROBLEM)
            dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM,null);
        else dialogHelper.showGeneralError();
    }

    JustifiedTextView.IJustifyTextViewListener justifyTextViewListener = new JustifiedTextView.IJustifyTextViewListener() {
        @Override
        public void onContentScroll(boolean hitToBottom) {
        }

        @Override
        public void onHyperLinkClicked(String url) {
            new DialogHelper(MasterCardUtilityActivity.this).showLeavingAlert(url);
        }
    };

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }
}
