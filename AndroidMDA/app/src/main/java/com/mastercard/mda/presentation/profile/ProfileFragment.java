package com.mastercard.mda.presentation.profile;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mastercard.mda.R;
import com.mastercard.mda.common.logic.MaskerHelper;
import com.mastercard.mda.common.logic.Validator;
import com.mastercard.mda.domain.model.Profile;
import com.mastercard.mda.presentation.base.BaseFragment;
import com.mastercard.mda.presentation.widget.CustomSpinner;
import com.mastercard.mda.presentation.widget.DropdownAdapter;
import com.mastercard.mda.presentation.widget.PasswordInputFilter;
import com.mastercard.mda.presentation.widget.ViewScrollViewListener;
import com.mastercard.mda.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public class ProfileFragment extends BaseFragment {

    enum PROFILE {CREATE, EDIT}

    private static final String ARG_WHICH = "which";
    @BindView(R.id.profile_rootview)
    LinearLayout llProfileParentView;
    @BindView(R.id.scroll_view)
    ScrollView mainScrollView;
    @BindView(R.id.llParent)
    LinearLayout llParent;
    @BindView(R.id.fakeViewToMoveButtonDown)
    View fakeViewToMoveButtonDown;
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.edt_first_name)
    ErrorIndicatorEditText edtFirstName;
    @BindView(R.id.edt_last_name)
    ErrorIndicatorEditText edtLastName;
    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.flEmailMask)
    View emailMask;
    @BindView(R.id.edt_phone)
    ErrorIndicatorEditText edtPhone;
    @BindView(R.id.edt_passcode)
    ErrorIndicatorEditText edtPassCode;
    @BindView(R.id.edt_pwd)
    ErrorIndicatorEditText edtPassword;
    @BindView(R.id.edt_pwd_confirm)
    ErrorIndicatorEditText edtConfirmPassword;
    @BindView(R.id.switch_location)
    SwitchCompat locationSwitchCompat;
    @BindView(R.id.checkbox_acknowledge)
    AppCompatCheckBox chbAcknowledge;
    @BindView(R.id.btn_submit)
    AppCompatButton btnSubmit;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.tvAcknow)
    LetterSpacingTextView tvAcknow;
    @BindView(R.id.tvSelectSalutation)
    LetterSpacingTextView tvSelectSalutation;
    @BindView(R.id.vSalutation)
    View viewSalutation;
    /* @BindView(R.id.spSalutation)
     Spinner spSalutation;*/
    private ProfileEventsListener listener;
    private Profile profile = new Profile();
    private ProfileComparator profileComparator;
    private Validator validator = new Validator();
    private FocusChangeListener firstNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener lastNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener emailMasker = new FocusChangeListener(MaskerHelper.INPUT.EMAIL);
    private FocusChangeListener phoneMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private TextWatcher phonePattern;
    private AppCompatEditText editText;
    private String errMessage;
    private int hasFocus;
    PROFILE which;
    private boolean isKeyboardShow;
    private CustomSpinner mSpinnerSalutation;
    private boolean isEdtMode;
    private boolean profileFromUpdate = true;
    private boolean isShowLocationAlert = true;

    public static ProfileFragment newInstance(PROFILE profile) {
        Bundle args = new Bundle();
        args.putString(ARG_WHICH, profile.name());
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        isShowLocationAlert = true;
        if (context instanceof ProfileEventsListener) {
            listener = (ProfileEventsListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement ProfileEventsListener.");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        storeUp();
        outState.putParcelable("input", profile);
        if (profileComparator != null)
            outState.putParcelable("original", profileComparator.original);
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listener.onFragmentCreated();
        which = PROFILE.valueOf(getArguments().getString(ARG_WHICH));
        if (which == PROFILE.EDIT) {
            isEdtMode = true;
            edtEmail.setTextColor(Color.parseColor("#99A1A8"));
            edtEmail.setInputType(InputType.TYPE_NULL);
            edtEmail.setCursorVisible(false);
            emailMask.setVisibility(View.VISIBLE);
            edtEmail.setEnabled(false);
            edtPassword.setVisibility(View.GONE);
            edtConfirmPassword.setVisibility(View.GONE);
            view.findViewById(R.id.checkbox_container).setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            btnSubmit.setText(getString(R.string.update));
            btnSubmit.setEnabled(false);
            btnSubmit.setClickable(false);
            btnCancel.setEnabled(false);
            btnCancel.setClickable(false);
            edtPassCode.setVisibility(View.GONE);
            if(getActivity() instanceof MyProfileActivity){
                View toolbar = ((MyProfileActivity) getActivity()).getViewToolbar();
                if(toolbar != null){
                    toolbar.setClickable(true);
                    toolbar.setOnClickListener(this::handleClickOutsideEdt);
                }
            }
        } else {
            btnSubmit.setEnabled(false);
            btnSubmit.post(new Runnable() {
                @Override
                public void run() {
                    btnSubmit.setClickable(false);
                }
            });
            edtPhone.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            tvSelectSalutation.setVisibility(View.VISIBLE);
            InputFilter[] filtersPassword = new InputFilter[]{new PasswordInputFilter(), new InputFilter.LengthFilter(getResources().getInteger(R.integer.password_max_length_characters))};
            edtPassword.setFilters(filtersPassword);
            edtConfirmPassword.setFilters(filtersPassword);
        }
        mSpinnerSalutation = setUpCustomSpinner(tvSelectSalutation, getSalutation());
        mSpinnerSalutation.setNone(false);
        edtFirstName.setOnFocusChangeListener(firstNameMasker);
        edtLastName.setOnFocusChangeListener(lastNameMasker);
        if(which == PROFILE.CREATE) {
            edtEmail.setOnFocusChangeListener(emailMasker);
        }
        edtPhone.setOnFocusChangeListener(phoneMasker);
        initTextInteractListener();
//        phonePattern = new TextPattern(edtPhone,
//                "#-###-###-####",
//                2);





        phonePattern = new TextWatcher() {
            String strBefore = "";
            int selection;
            @Override
            public void beforeTextChanged(CharSequence s, int index, int before, int count) {
                strBefore = s.toString();
                selection = edtPhone.getSelectionEnd();
            }

            @Override
            public void onTextChanged(CharSequence sequence, int index, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtPhone.removeTextChangedListener(phonePattern);
                String text = editable.toString();
                if (text.length() == 1 || text.length() == Profile.PHONE_MAX_LENGTH) {
                    int maxLength = (text.charAt(0) == '+') ? Profile.PHONE_LENGTH_ACCEPT : Profile.PHONE_MAX_LENGTH;
                    handleInputPhone(maxLength);
                }

                if(text.lastIndexOf("+") > 0){
                    edtPhone.setText(strBefore);
                    edtPhone.setSelection(selection);
                }
                edtPhone.addTextChangedListener(phonePattern);
            }
        };
        //edtPhone.addTextChangedListener(phonePattern);
        chbAcknowledge.setOnCheckedChangeListener((buttonView, isChecked) -> {
            btnSubmit.setEnabled(isChecked);
            btnSubmit.setClickable(isChecked);});
        locationSwitchCompat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean on = !locationSwitchCompat.isChecked();
                if (which == PROFILE.EDIT && profileComparator != null) profileComparator.onChanged();
                if (on) listener.onLocationSwitchOn();
            }
        });

        tvAcknow.setOnClickListener(view1 -> chbAcknowledge.setChecked(!chbAcknowledge.isChecked()));
        if (savedInstanceState != null) {
            loadProfile(savedInstanceState.getParcelable("original"), false);
        }
        ClickGuard.guard(btnSubmit, btnCancel);
        llParent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if(focus){
                    hideSoftKey();
                }
            }
        });
        new ViewScrollViewListener(mainScrollView, this::handleClickOutsideEdt);
    }

    private List<String> getSalutation() {
        return Arrays.asList(getResources().getStringArray(R.array.cate_salutations));
    }

    private CustomSpinner setUpCustomSpinner(TextView anchor,
                                             List<String> data) {
        CustomSpinner customSpinner = new CustomSpinner(getContext(), anchor);

        DropdownAdapter dropdownAdapter = new DropdownAdapter(getContext(),
                R.layout.item_dropdown,
                data);
        customSpinner.setDropdownAdapter(dropdownAdapter);
        int maxHeight = data.size() * 40;
        if (maxHeight < 280) {
            customSpinner.setPobpupHeight(maxHeight);
        } else {
            customSpinner.setPobpupHeight(280);
        }
        return customSpinner;
    }

    private void initTextInteractListener() {
        CopyPasteHandler copyPasteHandler = new CopyPasteHandler(validator);
        //everytime user paste text to edittext, onTextPasted get called.
//        edtFirstName.setTextInteractListener(copyPasteHandler);
//        edtLastName.setTextInteractListener(copyPasteHandler);
//        edtPhone.setTextInteractListener(copyPasteHandler);
        llProfileParentView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            llProfileParentView.getWindowVisibleDisplayFrame(r);
            int screenHeight = llProfileParentView.getRootView().getHeight();

            int keypadHeight = screenHeight - r.bottom;

            if (keypadHeight > screenHeight * 0.15) {
                if (!isKeyboardShow) {
                    isKeyboardShow = true;
                    // keyboard is opened
                    mainScrollView.smoothScrollTo(0, (int) contentWrapper.getTop());
                    scaleView();
                    processAfterKeyboardToggle(true);
                }
            } else {
                // keyboard is closed
                if (isKeyboardShow) {
                    isKeyboardShow = false;
                    fakeViewToMoveButtonDown.setVisibility(View.VISIBLE);
                    processAfterKeyboardToggle(false);
                }
            }
        });
    }

    private void scaleView() {
        Animation anim = new ScaleAnimation(
                1f, 1f, // Start and end values for the X axis scaling
                fakeViewToMoveButtonDown.getY(),
                fakeViewToMoveButtonDown.getY() + fakeViewToMoveButtonDown.getHeight(), // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        anim.setFillAfter(false); // Needed to keep the result of the animation
        anim.setDuration(0);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fakeViewToMoveButtonDown.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        fakeViewToMoveButtonDown.startAnimation(anim);
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        profileFromUpdate = true;
        hideSoftKey();
        storeUp();
        hasFocus = checkHasFocus();
        if (formValidation()) {
            // Replace the "+" sign of phone number
            profile.setPhone(profile.getPhone().replace("+", ""));
            profile.locationToggle(!locationSwitchCompat.isChecked());
            if (which == PROFILE.EDIT) {
                ifValidedPassCode();
            } else {
                listener.onCheckPassCodeSubmit(edtPassCode.getText().toString());
            }
        }

    }

    void ifValidedPassCode() {
        listener.onProfileSubmit(profile, edtPassword.getText().toString(), edtPassCode.getText().toString());
    }

    void onCheckPasscodeFail() {
        edtPassCode.setText("");
        edtPassCode.setError("");
        edtPassCode.requestFocus();
        edtPassCode.setSelection(edtPassCode.getText().toString().length());
    }

    @OnClick(R.id.btn_cancel)
    void onCancel(View view) {
        profileFromUpdate = false;
        hideSoftKey();
        hasFocus = checkHasFocus();
        resetErrorView();
        listener.onProfileCancel();
    }

    @OnClick({R.id.profile_rootview, R.id.content_wrapper, R.id.flEmailMask, R.id.llParent, R.id.scroll_view, R.id.contentSubmit, R.id.fakeViewToMoveButtonDown})
    public void onClick(View view) {
        handleClickOutsideEdt(view);
    }

    private void handleClickOutsideEdt(View view){
        hideSoftKey();
        if (view.getId() == R.id.flEmailMask) {
            edtEmail.setText(emailMasker.original);
        } else {
            if (edtEmail != null && emailMasker != null &&
                    !TextUtils.isEmpty(edtEmail.getText().toString()) &&
                    edtEmail.getText().toString().equalsIgnoreCase(emailMasker.original)) {

                edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));
            }//do nothing
        }
    }

    private void hideSoftKey() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void processAfterKeyboardToggle(boolean isKeyboardShow) {
        View currentView = getActivity().getCurrentFocus();
        if (currentView != null && currentView instanceof EditText) {
            if (isKeyboardShow) {
                ((EditText) currentView).setCursorVisible(true);
            }else{
                currentView.clearFocus();
                llParent.requestFocus();
            }
        }
    }

    private boolean formValidation() {
        checkout();
        if (editText != null) {
            listener.onProfileInputError(errMessage);
            return false;
        } else if (tvSelectSalutation.getText().toString().equals(getString(R.string.my_profile_text_salutation_place_holder))) {
            errMessage = getString(R.string.input_err_salutation);
            listener.onProfileInputError(errMessage);
            return false;
        }
        return true;
    }

    private void checkout() {
        editText = null;
        StringBuilder messageBuilder = new StringBuilder();
        if (tvSelectSalutation.getText().toString().equals(getString(R.string.my_profile_text_salutation_place_holder))) {
            viewSalutation.setBackgroundColor(Color.parseColor("#ffff4444"));
            if (messageBuilder.length() > 0) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(getString(R.string.input_err_salutation));
        } else {
            viewSalutation.setBackgroundColor(Color.parseColor("#99A1A8"));
        }

        if (!validator.name(profile.getFirstName())) {
            editText = edtFirstName;
            edtFirstName.setError("");
            if (profile.getFirstName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_first_name));
            }

        } else if (!validator.specialChars(profile.getFirstName())) {
            editText = edtFirstName;
            edtFirstName.setError("");
            if (profile.getFirstName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_special_char_first_name));
            }
        } else {
            edtFirstName.setError(null);
        }

        if (!validator.name(profile.getLastName())) {
            if (editText == null) editText = edtLastName;
            edtLastName.setError("");
            if (profile.getLastName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_last_name));
            }

        } else if (!validator.specialChars(profile.getLastName())) {
            if(editText == null)editText = edtLastName;
            edtLastName.setError("");
            if (profile.getLastName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_special_char_last_name));
            }
        } else {
            edtLastName.setError(null);
        }
        if (!validator.email(profile.getEmail())) {
            if (editText == null) editText = edtEmail;
            edtEmail.setError("");
            if (profile.getEmail().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_email));
            }
        } else {
            edtEmail.setError(null);
        }
        //-- phone
        String phoneOnView;
        if(edtPhone.hasFocus()){
            phoneOnView = edtPhone.getText().toString().trim();
        }else {
            phoneOnView = phoneMasker.original.trim();
        }
        if (!validator.phoneFormatValidator(phoneOnView)) {
            if (editText == null) editText = edtPhone;
            edtPhone.setError("");
            if (profile.getPhone()
                    .length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_phone));
            }
        } else {
            String phoneWithOnlyDigit = profile.getPhone().replace("+", "");
            String foundCountryCode = validator.findCountryCodeFromScratch(phoneWithOnlyDigit);
            if(TextUtils.isEmpty(foundCountryCode)){
                if (editText == null) editText = edtPhone;
                edtPhone.setError("");
                if (profile.getPhone()
                        .length() > 0) {
                    if (messageBuilder.length() > 0) {
                        messageBuilder.append("\n");
                    }
                    messageBuilder.append(getString(R.string.input_err_invalid_country_code));
                }
            }else{
                if(phoneWithOnlyDigit.equalsIgnoreCase(foundCountryCode)){ // Full phone with only country code
                    if (editText == null) editText = edtPhone;
                    edtPhone.setError("");
                    if (profile.getPhone()
                            .length() > 0) {
                        if (messageBuilder.length() > 0) {
                            messageBuilder.append("\n");
                        }
                        messageBuilder.append(getString(R.string.input_err_invalid_phone));
                    }
                }else{
                    edtPhone.setError(null);
                }
            }
        }

        String password = edtPassword.getText().toString();
        String reTypePwd = edtConfirmPassword.getText().toString();
        if (which == PROFILE.CREATE) {
            if (TextUtils.isEmpty(edtPassCode.getText())) {
                if (editText == null) {
                    editText = edtPassCode;
                }
                edtPassCode.setError("");
            } else {
                edtPassCode.setError(null);
            }

            if (!validator.secretValidator(password)) {
                if (editText == null) {
                    editText = edtPassword;
                }
                edtPassword.setError("");
                if (password.length() > 0) {
                    if (messageBuilder.length() > 0) {
                        messageBuilder.append("\n");
                    }
                    messageBuilder.append(getString(R.string.input_err_invalid_password));
                }
            } else {
                edtPassword.setError(null);
            }

            if (!validator.secretValidator(reTypePwd)) {
                if (editText == null) {
                    editText = edtConfirmPassword;
                }
                edtConfirmPassword.setError("");
            }
            boolean emptyPassword =  reTypePwd.length() == 0;
            if (!emptyPassword) {
                if (!validator.password(password, reTypePwd)) {
                    if (editText == null) {
                        editText = edtConfirmPassword;
                    }
                    edtConfirmPassword.setError("");

                    if (messageBuilder.length() > 0) {
                        messageBuilder.append("\n");

                    }

                    messageBuilder.append(getString(R.string.input_err_pwd_confirmation_failed));
                } else {
                    edtConfirmPassword.setError(null);
                }
            }
        }
        if (which == PROFILE.CREATE && editText == null && password.length() == 0 && edtPassCode.getText().length() == 0) {
            editText = edtPassword;
            editText = edtPassCode;
        }
        /*if (editText != null) {
            if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus() instanceof ErrorIndicatorEditText) {
                if (((ErrorIndicatorEditText) getActivity().getCurrentFocus()).isError()) {
                    editText = (ErrorIndicatorEditText) getActivity().getCurrentFocus();
                }
            }
        }*/
        if (editText != null) {
            String temp = messageBuilder.toString();
            messageBuilder = new StringBuilder("");
            if (which == PROFILE.CREATE && profile.anyEmpty(edtPassword, edtConfirmPassword, edtPassCode)) {
                messageBuilder.append(getString(R.string.input_err_required));
            } else if (profile.anyIsEmpty()) {
                messageBuilder.append(getString(R.string.input_err_required));
            }

            if (messageBuilder.length() > 0) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(temp);

        }
        errMessage = messageBuilder.toString();
    }

    private class FocusChangeListener
            implements View.OnFocusChangeListener {

        MaskerHelper.INPUT input;
        private String original = "";

        FocusChangeListener(MaskerHelper.INPUT type) {
            input = type;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            AppCompatEditText view = (AppCompatEditText) v;
            /*if (input == MaskerHelper.INPUT.PHONE) {
                if (hasFocus) {
                    if (original.length() == 0) {
                        //view.removeTextChangedListener(phonePattern);
                        view.setText(original);
                        view.setSelection(2);
                        //view.addTextChangedListener(phonePattern);
                        return;
                    }
                } else {
                    String text = view.getText().toString();
                    if (text.length() == 2) {
                        //view.removeTextChangedListener(phonePattern);
                        view.setText(original);
                        //view.addTextChangedListener(phonePattern);
                        return;
                    }
                }
            }*/
            if (hasFocus) {
                view.setText(original);
                if (isEdtMode && view != edtEmail) {
                    edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));
                }

            } else {

                String text = view.getText().toString().trim();
                original = text;
                if (view == edtPhone) {
                    if (text.length() > 0 ) {
                        int maxLength = ((text.length() == Profile.PHONE_MAX_LENGTH && text.charAt(0) != '+') || (text.charAt(0) == '+'))
                                ? Profile.PHONE_LENGTH_ACCEPT : Profile.PHONE_MAX_LENGTH;
                        handleInputPhone(maxLength);
                        if (text.charAt(0) != '+') {
                            original = "+" + text;
                        }
                    }
                }
                view.setText(MaskerHelper.mask(input, original));
            }
            view.setCursorVisible(hasFocus);

        }
    }

    private void handleInputPhone(int maxLength){
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        edtPhone.setFilters(fArray);
    }

    private void storeUp() {
        firstNameMasker.original = firstNameMasker.original.trim();
        lastNameMasker.original = lastNameMasker.original.trim();
        emailMasker.original = emailMasker.original.trim();
        phoneMasker.original = phoneMasker.original.trim();
        String firstName = firstNameMasker.original;
        String lastName = lastNameMasker.original;
        String email = emailMasker.original;
        String phone = phoneMasker.original;
        profile.setFirstName(firstName);
        profile.setLastName(lastName);
        profile.setPhone(phone);
        profile.setEmail(email);
        profile.setSalutation(getSalutation().get(mSpinnerSalutation.getSelectionPosition())/*spSalutation.getSelectedItem()*/);
    }

    private int checkHasFocus() {
        if (edtFirstName.hasFocus()) {
            profile.setFirstName(edtFirstName.getText().toString().trim());
            return 1;
        }
        if (edtLastName.hasFocus()) {
            profile.setLastName(edtLastName.getText().toString().trim());
            return 2;
        }
        if (edtEmail.hasFocus()) {
            if(which == PROFILE.CREATE) {
                profile.setEmail(edtEmail.getText().toString().trim());
            }
            return 3;
        }
        if (edtPhone.hasFocus()) {
            profile.setPhone(edtPhone.getText().toString().trim());
            return 4;
        }
        // zip code
        return 5;
    }
    void reloadProfileAfterUpdated(){
        loadProfile(profile, false);
    }
    void loadProfile(Profile profile, boolean fromRemote) {
        if(profile == null){
            return;
        }
        // Always add "+" at the first place in phone number
        final String phoneOnView = profile.getDisplayCountryCodeAndPhone();
//        profile.addPlusSignAtTheFirstIfAny();

        firstNameMasker.original = profile.getFirstName();
        lastNameMasker.original = profile.getLastName();
        phoneMasker.original = phoneOnView;
        emailMasker.original = profile.getEmail();
        edtFirstName.setText(profile.getFirstName());
        edtLastName.setText(profile.getLastName());
        edtEmail.setText(profile.getEmail());
        edtPhone.setText(phoneOnView);
        if(profileFromUpdate) {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, false);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, false);
            //edtEmail.getOnFocusChangeListener().onFocusChange(edtEmail, false);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, false);
        }else{
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, hasFocus == 1);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, hasFocus == 2);
            //edtEmail.getOnFocusChangeListener().onFocusChange(edtEmail, hasFocus == 3);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, hasFocus == 4);
        }
        edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));

        if (profile.getSalutation() != null && !profile.getSalutation().equals("")) {
            if (getSalutation().contains(profile.getSalutation())) {
                mSpinnerSalutation.setSelectionPosition(getSalutation().indexOf(profile.getSalutation()));
            }
        }
        profileComparator = new ProfileComparator(
                edtFirstName,
                edtLastName,
                edtEmail,
                edtPhone,
                btnSubmit,
                btnCancel,
                locationSwitchCompat,
                mSpinnerSalutation);
        profileComparator.original = profile;
        boolean on = profile.isLocationOn();
        locationSwitchCompat.setChecked(!on);
        if(fromRemote) {
            if (on && isShowLocationAlert) {
                listener.onLocationSwitchOn();
            }
            isShowLocationAlert = false;
        }
        resetErrorView();

        btnSubmit.setEnabled(false);
        btnSubmit.setClickable(false);
        btnCancel.setEnabled(false);
        btnCancel.setClickable(false);
    }

    void invokeSoftKey() {
        if (profileComparator != null && which == PROFILE.EDIT) {
            //prevent text change
            profileComparator.removeTextChangeListener();
            showFocusError();
            profileComparator.addTextChangeListener();
        } else {
            //focus error create account
            showFocusError();
        }
        ViewUtils.showSoftKey(editText != null ? editText : llProfileParentView);
    }

    private void showFocusError() {
        if (editText != null) {
            editText.requestFocus();
            editText.setError("");
            editText.setCursorVisible(true);
            editText.setSelection(editText.getText().toString().length());
        }
    }

    void switchOff() {
        locationSwitchCompat.setChecked(true);
        profile.locationToggle(false);
        if (which == PROFILE.EDIT) {
            profileComparator.onChanged();
        }
    }

    public interface ProfileEventsListener {
        void onCheckPassCodeSubmit(String passCode);

        void onProfileSubmit(Profile profile, String password, String passCode);

        void onProfileInputError(String message);

        void onProfileCancel();

        void onFragmentCreated();

        void onLocationSwitchOn();
    }

    private void resetErrorView() {
        edtFirstName.setError(null);
        edtLastName.setError(null);
        edtEmail.setError(null);
        edtPhone.setError(null);
        edtPassword.setError(null);
        edtPassCode.setError(null);
    }

    public void showHighlightFieldEmail(){
        if(edtEmail != null){
            edtEmail.requestFocus();
            edtEmail.setError("");
            edtEmail.setCursorVisible(true);
            edtEmail.setSelection(edtEmail.getText().toString().length());
            ViewUtils.showSoftKey(edtEmail);
        }
    }

}
