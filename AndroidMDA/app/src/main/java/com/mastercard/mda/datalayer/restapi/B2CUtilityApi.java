package com.mastercard.mda.datalayer.restapi;

import com.mastercard.mda.datalayer.entity.b2cutility.B2CGetMasterCardCopyRequest;
import com.mastercard.mda.datalayer.entity.b2cutility.B2CGetMasterCardCopyResponse;
import com.mastercard.mda.datalayer.entity.search.SearchRequest;
import com.mastercard.mda.datalayer.entity.search.SearchResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * for all other categories business except Dining
 */
public interface B2CUtilityApi {

    @POST("utility/GetClientCopy")
    Call<B2CGetMasterCardCopyResponse> getMasterCardCopy(@Body B2CGetMasterCardCopyRequest body);

    @POST("utility/SearchIAAndCCA")
    Call<SearchResponse> searchByKeyword(@Body SearchRequest body);

}
