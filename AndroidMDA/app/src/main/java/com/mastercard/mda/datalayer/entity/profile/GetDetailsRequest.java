package com.mastercard.mda.datalayer.entity.profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class GetDetailsRequest {

    @SerializedName("AccessToken")
    private String accessToken;
    @SerializedName("ConsumerKey")
    private String consumerKey;
    @SerializedName("Functionality")
    private String functionality;
    @SerializedName("OnlineMemberId")
    private String onlineMemberID;
    @SerializedName("MemberRefNo")
    private String memberRefNo;

    public GetDetailsRequest(String accessToken, String onlineMemberID) {
        this.accessToken = accessToken;
        this.consumerKey = "DMA";
        this.functionality = "GetUserDetails";
        this.onlineMemberID = onlineMemberID;
        this.memberRefNo = "TestMemRef127";
    }
}
