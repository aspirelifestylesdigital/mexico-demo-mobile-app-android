package com.mastercard.mda.presentation.profile;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.mastercard.mda.App;
import com.mastercard.mda.R;
import com.mastercard.mda.common.GPSChecker;
import com.mastercard.mda.common.constant.AppConstant;
import com.mastercard.mda.common.constant.ResultCode;
import com.mastercard.mda.domain.model.Profile;
import com.mastercard.mda.presentation.LocationPermissionHandler;
import com.mastercard.mda.presentation.base.CommonActivity;
import com.mastercard.mda.presentation.widget.DialogHelper;
import com.mastercard.mda.presentation.widget.ViewUtils;

public class MyProfileActivity extends CommonActivity implements ProfileFragment.ProfileEventsListener,
        MyProfile.View {

    private EditProfilePresenter presenter;
    private DialogHelper dialogHelper;
    private ProfileFragment myFragment;
    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    private EditProfilePresenter editProfilePresenter() {
        return new EditProfilePresenter(getApplicationContext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.acticity_dummy_content);
        setTitle(R.string.profile_edit_title);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , ProfileFragment.newInstance(ProfileFragment.PROFILE.EDIT)
                            , ProfileFragment.class.getSimpleName())
                    .commit();
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialogHelper = new DialogHelper(this);
        presenter = editProfilePresenter();
        presenter.attach(this);

        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.MY_PROFILE.getValue());
        }
    }

    protected View getViewToolbar() {
        return toolbar;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void onFragmentCreated() {
        presenter.getProfileLocal();
        presenter.getProfile();
    }

    @Override
    public void onLocationSwitchOn() {
        if (!locationPermissionHandler.hasPermission(this)) {
            locationPermissionHandler.requestPermission(this);
        } else {
            if (!GPSChecker.GPSEnable(getApplicationContext())) {
                dialogHelper.action(null, getApplicationContext().getString(R.string.dialogLocationContent),
                        getApplicationContext().getString(R.string.textSetting), getApplicationContext().getString(R.string.text_cancel),
                        (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
        if (!granted) myFragment.switchOff();
        else if (!GPSChecker.GPSEnable(getApplicationContext())) {
            dialogHelper.action(null, getApplicationContext().getString(R.string.dialogLocationContent),
                    getApplicationContext().getString(R.string.textSetting), getApplicationContext().getString(R.string.text_cancel),
                    (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
        }
    }

    @Override
    public void onCheckPassCodeSubmit(String passCode) {

    }

    @Override
    public void onProfileSubmit(Profile profile, String password, String passCode) {
        setResult(ResultCode.RESULT_OK);
        presenter.updateProfile(profile);
    }

    @Override
    public void onProfileCancel() {
        presenter.getProfileLocal();
    }

    @Override
    public void showProfile(Profile profile, boolean fromRemote) {
        myFragment = (ProfileFragment) getSupportFragmentManager()
                .findFragmentByTag(ProfileFragment.class.getSimpleName());
        myFragment.loadProfile(profile, fromRemote);
    }

    @Override
    public void profileUpdated() {
        dialogHelper.alert(null, getString(R.string.profile_updated_message));
        if (myFragment != null) {
            myFragment.reloadProfileAfterUpdated();
        }
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if (ErrorApi.isGetTokenError(extraMsg)) dialogHelper.showGetTokenError();
//      else if(errCode == ErrCode.API_ERROR) dialogHelper.alert(getString(R.string.dialogTitleError), extraMsg);
        else dialogHelper.showGeneralError();
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgressCancelableUnEnable();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (getCurrentFocus() != null) {
                ViewUtils.hideSoftKey(getCurrentFocus());
            }
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message, dialog -> {
            if (myFragment != null) {
                myFragment.getView().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        myFragment.invokeSoftKey();
                    }
                }, 100);

            }
        });
    }
}
