package com.mastercard.mda.presentation.preferences;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.entity.preference.PreferenceData;
import com.mastercard.mda.presentation.base.BasePresenter;

public interface UserPreferences {

    interface View {
        void loadPreferencesOnUI(PreferenceData data);
        void showPreferenceSavedDialog();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void hideLoading();
        void showLoading();
    }

    interface Presenter extends BasePresenter<View> {
        void savePreferences(PreferenceData preferenceData);
        void loadPreferences();
    }
}
