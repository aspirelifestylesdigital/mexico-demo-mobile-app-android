package com.mastercard.mda.presentation.profile;

import android.content.Context;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.domain.usecases.CreateProfileCase;
import com.mastercard.mda.App;
import com.mastercard.mda.domain.mapper.profile.MapProfile;
import com.mastercard.mda.domain.model.Profile;
import com.mastercard.mda.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/28/2017.
 */

public class CreateProfilePresenter implements CreateProfile.Presenter {

    private CompositeDisposable disposables;
    private CreateProfile.View view;
    private MapProfile mapProfile;
    private CreateProfileCase createProfileCase;

    CreateProfilePresenter(Context context) {
        disposables = new CompositeDisposable();
        this.mapProfile = new MapProfile();
        this.createProfileCase = new CreateProfileCase(context, new MapProfileApp());
    }

    @Override
    public void attach(CreateProfile.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        view = null;
    }

    @Override
    public void createProfile(Profile profile, String password, String passCode) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        CreateProfileCase.Params params = mapProfile.createProfile(profile, password, passCode);
        disposables.add(
                createProfileCase.buildUseCaseCompletable(params)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new SaveProfileObserver()));
    }

    @Override
    public void abort() {
        disposables.clear();
    }

    private final class SaveProfileObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            if (view == null) return;
            view.dismissProgressDialog();
            view.showProfileCreatedDialog();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view == null) return;
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }

}
