package com.mastercard.mda.common.constant;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public enum  ErrCode {
    CONNECTIVITY_PROBLEM,
    API_ERROR,
    UNKNOWN_ERROR
}
