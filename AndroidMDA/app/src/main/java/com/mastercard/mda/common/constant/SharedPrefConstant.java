package com.mastercard.mda.common.constant;

/**
 * Created by tung.phan on 5/5/2017.
 */

public interface SharedPrefConstant {

    String PROFILE = "ppfile";
    String PREFERENCES = "preferences";
    String METADATA = "pMetadata";
    String SELECTED_CITY = "cityNameSelected";
    String FORGOT_SECRET_KEY = "hasFP";
}
