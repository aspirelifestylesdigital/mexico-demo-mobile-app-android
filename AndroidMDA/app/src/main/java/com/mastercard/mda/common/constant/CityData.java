package com.mastercard.mda.common.constant;

import android.text.TextUtils;
import android.util.SparseArray;

import com.mastercard.mda.App;
import com.mastercard.mda.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vinh.trinh on 6/2/2017.
 */

public final class CityData {
    /* warring change array cities */
    private static LinkedHashMap<String, City> cities;
    static final SparseArray<CityGuide> cityGuide;
    private static String SELECTED_CITY = "";
    public static final List<String> regions = Arrays.asList("África / Medio Oriente", "América del Sur y Central", "Asia Pacífico",
            "Canadá", "Caribe", "Estados Unidos", "Europa");

    //<editor-fold desc="Value config cca SubCategory and geographic region">
    public static final String VALUE_SUB_CCA_CATEGORY_ANY = "Any";
    public static final String VALUE_GEOGRAPHIC_REGION_ALL = "All";
    public static final String VALUE_GEOGRAPHIC_ALL_EXCEPT_CITY = "All_Except_City";
    //</editor-fold>

    //-- Fix #UDA-203
    // Update Data add ,1086,1370,1371,1372,1373,1374,1375;
    /**
     * case: id in array, app skip process that object for get Category or SearchResult
     */
    public static final List<Integer> cityGuideCodeList = Arrays.asList(7119, 7120, 7121, 7122, 7123,
            7077, 7078, 7079, 7081, 7080,
            7124, 7125, 7126, 7127, 7128,
            7082, 7083, 7084, 7085, 7086,
            6984, 6985, 6986, 6987, 6988,
            7134, 7135, 7136, 7137, 7138,
            6996, 6997, 6998, 6999, 7000,
            7087, 7088, 7089, 7090, 7091,
            7139, 7140, 7141, 7142, 7143,
            7092, 7093, 7094, 7095, 7096,
            7144, 7145, 7146, 7147, 7148,
            7149, 7150, 7151, 7152, 7153,
            7164, 7165, 7166, 7167, 7168
    );

    static {
        cityGuide = new SparseArray<>();

        //<editor-fold desc="Config filter ccaSubCategory and Geographic">

        List<CityCountry> cityBoston = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Massachusetts.New Hampshire.Rhode Island")
        ));

        List<CityCountry> cityCanCun = new ArrayList<>(Arrays.asList(
                new CityCountry("Caribbean/Bahamas/Mexico", "Quintana Roo"),
                new CityCountry("Latin America (South/Central America)", "Quintana Roo")
        ));

        List<CityCountry> cityChicago = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Illinois")
        ));

        List<CityCountry> cityGuadalajara = new ArrayList<>(Arrays.asList(
                new CityCountry("Caribbean/Bahamas/Mexico", "Jalisco"),
                new CityCountry("Latin America (South/Central America)", "Jalisco")
        ));

        List<CityCountry> cityLasVegas = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Arizona.Nevada")
        ));

        List<CityCountry> cityLondon = new ArrayList<>(Collections.singletonList(
                new CityCountry("Europe", "United Kingdom")
        ));

        List<CityCountry> cityLosAngele = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "California")
        ));

        List<CityCountry> cityLosCabos = new ArrayList<>(Arrays.asList(
                new CityCountry("Caribbean/Bahamas/Mexico", "Baja California Sur"),
                new CityCountry("Latin America (South/Central America)", "Baja California Sur")
        ));

        List<CityCountry> cityMexico = new ArrayList<>(Arrays.asList(
                new CityCountry("Caribbean/Bahamas/Mexico", "Mexico City"),
                new CityCountry("Latin America (South/Central America)", "Mexico City")
        ));

        List<CityCountry> cityMiami = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Florida")
        ));

        List<CityCountry> cityMonterrey = new ArrayList<>(Arrays.asList(
                new CityCountry("Caribbean/Bahamas/Mexico", "Nuevo León"),
                new CityCountry("Latin America (South/Central America)", "Nuevo León")
        ));

        List<CityCountry> cityNewYork = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Connecticut.New York")
        ));

        List<CityCountry> cityOrlando = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Florida")
        ));

        List<CityCountry> cityParis = new ArrayList<>(Collections.singletonList(
                new CityCountry("Europe", "France")
        ));

        List<CityCountry> cityRome = new ArrayList<>(Collections.singletonList(
                new CityCountry("Europe", "Italy")
        ));

        List<CityCountry> citySanFrancisco = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "California")
        ));

        List<CityCountry> cityAfrica = new ArrayList<>(Collections.singletonList(
                new CityCountry("Africa/Middle East", VALUE_GEOGRAPHIC_REGION_ALL)
        ));

        List<CityCountry> cityAmeriaca = new ArrayList<>(Arrays.asList(
                new CityCountry("Latin America (South/Central America)", VALUE_GEOGRAPHIC_ALL_EXCEPT_CITY,
                        "Mexico.Amazonas.Bahia.Chile.Ciudad Autónoma de Buenos Aires.Colombia.Pernambuco.Rio de Janeiro.Río Negro.Santa Catarina.São Paulo.Uruguay",
                        "Mexico City.Nuevo León.Quintana Roo.Jalisco.Baja California Sur"),
                new CityCountry("North America", "Mexico")
        ));

        List<CityCountry> cityAsia = new ArrayList<>(Collections.singletonList(
                new CityCountry("Asia Pacific", VALUE_GEOGRAPHIC_REGION_ALL)
        ));

        List<CityCountry> cityCanada = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "British Columbia.Canada.Ontario.Quebec")
        ));

        List<CityCountry> cityCaribe = new ArrayList<>(Arrays.asList(
                new CityCountry("Caribbean", VALUE_GEOGRAPHIC_REGION_ALL),
                new CityCountry("Caribbean/Bahamas/Mexico", VALUE_GEOGRAPHIC_ALL_EXCEPT_CITY,
                        "",
                        "Mexico.Mexico City.Baja California Sur.Jalisco.Nuevo León.Quintana Roo")
        ));

        List<CityCountry> cityUS = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Delaware.District of Columbia.Georgia.Hawaii.Lousiana.Maine.Texas.Vermont.Virgina.Wisconsin.Washington")
        ));

        List<CityCountry> cityEuropa = new ArrayList<>(Arrays.asList(
                new CityCountry("Europe", VALUE_GEOGRAPHIC_ALL_EXCEPT_CITY,
                        "France.United Kingdom.Italy",
                        "")
        ));
        //</editor-fold>

        cities = new LinkedHashMap<>();

        //<editor-fold desc="Config city name">
        final String cityNameBoston = "Boston";
        final String cityNameCancun = "Cancún";
        final String cityNameChicago = "Chicago";
        final String cityNameGuadalajara = "Guadalajara";
        final String cityNameLasVegas = "Las Vegas";
        final String cityNameLondres = "Londres";
        final String cityNameLosAngeles = "Los Ángeles";
        final String cityNameLosCabos = "Los Cabos";
        final String cityNameMexicoCity = "Ciudad de México";
        final String cityNameMiami = "Miami";
        final String cityNameMonterrey = "Monterrey";
        final String cityNameNewYork = "New York";
        final String cityNameOrlando = "Orlando";
        final String cityNameParís = "París";
        final String cityNameRoma = "Roma";
        final String cityNameSanFancisco = "San Francisco";
        //Region
        final String cityNameAfrica = "África / Medio Oriente";
        final String cityNameAmerica = "América del Sur y Central";
        final String cityNameAsia = "Asia Pacífico";
        final String cityNameCanada = "Canadá";
        final String cityNameCaribe = "Caribe";
        final String cityNameEU = "Estados Unidos";
        final String cityNameEuropa = "Europa";

        //</editor-fold>


        //Encode string city name
        final String cityEncodeNameAfrica = "A&#769;frica / Medio Oriente";
        final String cityEncodeNameAmerica = "Ame&#769;rica del sur y central";
        final String cityEncodeNameCanada = "Canada&#769;";
        final String cityEncodeNameCancun = "Cancu&#769;n";
        final String cityEncodeNameMexicoCity = "Ciudad de Me&#769;xico";
        final String cityEncodeNameLosAngeles = "Los A&#769;ngeles";
        final String cityEncodeNameParís = "Pari&#769;s";
        final String cityEncodeNameAsisa = "Asia Pacifico";


        cities.put(cityNameBoston, new City(cityNameBoston, false, cityBoston, true, 6958, 1, 7119, 7120, 7121, 7122, 7123, ""));

        cities.put(cityNameCancun, new City(cityNameCancun, true, cityCanCun, true, 6797, 2, 7077, 7078, 7079, 7081, 7080, cityEncodeNameCancun));

        cities.put(cityNameChicago, new City(cityNameChicago, false, cityChicago, true, 6847, 3, 7124, 7125, 7126, 7127, 7128, ""));

        cities.put(cityNameMexicoCity, new City(cityNameMexicoCity, true, cityMexico, true, 6833, 9, 7087, 7088, 7089, 7090, 7091, cityEncodeNameMexicoCity));

        cities.put(cityNameGuadalajara, new City(cityNameGuadalajara, true, cityGuadalajara, true, 6803, 4, 7082, 7083, 7084, 7085, 7086, ""));

        cities.put(cityNameLasVegas, new City(cityNameLasVegas, false, cityLasVegas, true, 6898, 5, 6984, 6985, 6986, 6987, 6988, ""));

        cities.put(cityNameLondres, new City(cityNameLondres, false, cityLondon, true, 6899, 0, ""));

        cities.put(cityNameLosAngeles, new City(cityNameLosAngeles, false, cityLosAngele, true, 6900, 7, 7134, 7135, 7136, 7137, 7138, cityEncodeNameLosAngeles));

        cities.put(cityNameLosCabos, new City(cityNameLosCabos, true, cityLosCabos, true, 6815, 8, 6996, 6997, 6998, 6999, 7000, ""));

        cities.put(cityNameMiami, new City(cityNameMiami, false, cityMiami, true, 6901, 10, 7139, 7140, 7141, 7142, 7143, ""));

        cities.put(cityNameMonterrey, new City(cityNameMonterrey, true, cityMonterrey, true, 6839, 11, 7092, 7093, 7094, 7095, 7096, ""));

        cities.put(cityNameNewYork, new City(cityNameNewYork, false, cityNewYork, true, 6902, 12, 7144, 7145, 7146, 7147, 7148, ""));

        cities.put(cityNameOrlando, new City(cityNameOrlando, false, cityOrlando, true, 6903, 13, 7149, 7150, 7151, 7152, 7153, ""));

        cities.put(cityNameParís, new City(cityNameParís, false, cityParis, true, 6904, 0, cityEncodeNameParís));

        cities.put(cityNameRoma, new City(cityNameRoma, false, cityRome, true, 6905, 0, ""));

        cities.put(cityNameSanFancisco, new City(cityNameSanFancisco, false, citySanFrancisco, true, 6906, 16, 7164, 7165, 7166, 7167, 7168, ""));
        //Region
        cities.put(cityNameAfrica, new City(cityNameAfrica, false, cityAfrica, false, 7098, 0, cityEncodeNameAfrica));
        cities.put(cityNameAmerica, new City(cityNameAmerica, false, cityAmeriaca, false, 7108, 0, cityEncodeNameAmerica));
        cities.put(cityNameAsia, new City(cityNameAsia, false, cityAsia, false, 6967, 0, cityEncodeNameAsisa));
        cities.put(cityNameCanada, new City(cityNameCanada, false, cityCanada, false, 7101, 0, cityEncodeNameCanada));
        cities.put(cityNameCaribe, new City(cityNameCaribe, false, cityCaribe, false, 7105, 0, ""));
        cities.put(cityNameEU, new City(cityNameEU, false, cityUS, false, 7103, 0, ""));
        cities.put(cityNameEuropa, new City(cityNameEuropa, false, cityEuropa, false, 7100, 0, ""));
    }

    public static void reset() {
        SELECTED_CITY = "";
    }

    public static boolean setSelectedCity(String cityName) {
        if (!TextUtils.isEmpty(cityName) && cities.get(cityName) != null) {
            SELECTED_CITY = cityName;
            return true;
        }//-- do nothing save select city
        return false;
    }

    /**
     * check city name exist in array city
     */
    public static boolean isCityInApp(String cityName) {
        if (!TextUtils.isEmpty(cityName) && cities.get(cityName) != null) {
            return true;
        }
        return false;
    }

    public static int selectedCity() {
        int pos = 0;
        for (Map.Entry<String, City> entry : cities.entrySet()) {
            if (SELECTED_CITY.equalsIgnoreCase(entry.getKey())) {
                break;
            }
            pos++;
        }
        return pos;
    }

    public static boolean citySelected() {
        return (!TextUtils.isEmpty(SELECTED_CITY) && cities.get(SELECTED_CITY) != null);
    }

    public static int diningCode() {
        if (!citySelected()) return 0;
        return cities.get(SELECTED_CITY).diningCode;
    }

    public static int guideCode() {
        if (!citySelected()) return 0;
        return cities.get(SELECTED_CITY).cityGuideCode;
    }

    /**
     * get city name on logic
     */
    public static String cityName() {
        if (!citySelected()) return "All";
        return cities.get(SELECTED_CITY).name;
    }

    public static String cityNameDefaultEmpty() {
        if (!citySelected()) return "";
        return cities.get(SELECTED_CITY).name;
    }

    /**
     * get city name encode on logic
     */
    public static String cityEncodeName() {
        if (!citySelected()) return "All";
        if (TextUtils.isEmpty(cities.get(SELECTED_CITY).encodeCityName)) {
            return cities.get(SELECTED_CITY).name;
        } else {
            return cities.get(SELECTED_CITY).encodeCityName;
        }
    }

    /**
     * is value city or region
     */
    public static boolean isCity() {
        return citySelected() && cities.get(SELECTED_CITY).isCity;
    }

    /**
     * is the city of America or Canada
     */
    public static boolean isCityOfUS() {
        return citySelected() && cities.get(SELECTED_CITY).isCityOfUS;
    }

    public static List<CityCountry> cityCountryList() {
        return cities.get(SELECTED_CITY).cityCountryList;
    }

    public static CityGuide cityGuide(int code) {
        return cityGuide.get(code);
        /*if(result == null) {
            throw new IllegalStateException("passed city code not found");
        }*/
    }

    /**
     * @param cityGuideCategoryIndex either position of spa | bar | shopping...
     * @return int value of
     */
    public static int specificCityGuideCode(int cityGuideCategoryIndex) {
        if (!citySelected())
            return -1;
        int cityCode = cities.get(SELECTED_CITY).cityGuideCode;
        if (cityCode == 0) return -1;
        CityGuide cityGuide = cityGuide(cityCode);
        if (cityGuide == null || cityGuide.size() <= cityGuideCategoryIndex) return -1;
        return cityGuide(cityCode).at(cityGuideCategoryIndex);
    }

    public static boolean isDiningItem(int subCategoryID) {
        for (Map.Entry<String, City> entry : cities.entrySet()) {
            City city = entry.getValue();
            if (city.diningCode == subCategoryID) return true;
        }
        return false;
    }

    public static boolean isCityGuideItem(int catID) {
        return catID > 0 && cityGuideCodeList.contains(catID);
    }
}
