package com.mastercard.mda.presentation.checkout;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.mastercard.mda.App;
import com.mastercard.mda.R;
import com.mastercard.mda.common.constant.AppConstant;
import com.mastercard.mda.common.constant.IntentConstant;
import com.mastercard.mda.presentation.base.BaseActivity;
import com.mastercard.mda.presentation.changepass.ChangePasswordActivity;
import com.mastercard.mda.presentation.home.HomeActivity;
import com.mastercard.mda.presentation.widget.DialogHelper;
import com.mastercard.mda.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by son.ho on 10/23/2017.
 */

public class NewPassCodeActivity extends BaseActivity implements PassCodeCheckout.View {

    @BindView(R.id.edt_passcode)
    ErrorIndicatorEditText edtPasscode;
    @BindView(R.id.btn_submit_passcode)
    Button btnPassCodeSubmit;
    PassCodeCheckoutPresenter passCodeCheckoutPresenter;
    DialogHelper dialogHelper;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    LetterSpacingTextView tittle;
    boolean navigateFromSignIn;

    private PassCodeCheckoutPresenter buildPassCodePresenter() {
        return new PassCodeCheckoutPresenter(getApplicationContext());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_passcode);
        ButterKnife.bind(this);
        tittle.setText("CONCIERGE");
        btnPassCodeSubmit.setEnabled(false);
        passCodeCheckoutPresenter = buildPassCodePresenter();
        passCodeCheckoutPresenter.attach(this);
        dialogHelper = new DialogHelper(this);

        edtPasscode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int count = editable.toString().length();
                if (count > 0) {
                    btnPassCodeSubmit.setEnabled(true);
                } else {
                    btnPassCodeSubmit.setEnabled(false);
                }
            }
        });
        // Get extra
        navigateFromSignIn = getIntent().getBooleanExtra(IntentConstant.REQUEST_PASSCODE_FROM_SIGNIN, false);
    }

    @OnClick(R.id.btn_submit_passcode)
    public void onSubmit(View view) {
        ViewUtils.hideSoftKey(view);
        String passCode = edtPasscode.getText().toString();
        if (!TextUtils.isEmpty(passCode)) {
            passCodeCheckoutPresenter.checkoutPossCode(passCode);
        }

    }


    /*hide keyboard when touch on screen*/
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                int posX = (int) ev.getRawX();
                int posY = (int) ev.getRawY();
                boolean edtRect = outRect.contains(posX, posY);
                if (!edtRect) {
                    v.clearFocus();
                    ViewUtils.hideSoftKey(v);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    void invokeSoftKey(View view) {
        if (edtPasscode != null) {
            edtPasscode.setText("");
            edtPasscode.requestFocus();
            edtPasscode.setCursorVisible(true);
            edtPasscode.setSelection(edtPasscode.getText().toString().length());
        }
        ViewUtils.showSoftKey(view);
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if (errCode == ErrCode.API_ERROR)
            dialogHelper.alert(getString(R.string.dialogTitleError), extraMsg);
        else dialogHelper.showGeneralError();
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void onCheckPassCodeSuccessfully() {
        String passCode = edtPasscode.getText().toString();
        if(!TextUtils.isEmpty(passCode)){
            passCodeCheckoutPresenter.handleSaveUserPrefPassCode(passCode);
        }
    }

    @Override
    public void updatePassCodeApiCompleted() {
        boolean hasForgotPwd = new PreferencesStorageAspire(getApplicationContext()).hasForgotPwd();
        Intent intent;
        if (hasForgotPwd) {
            intent = new Intent(this, ChangePasswordActivity.class);
            intent.putExtra(Intent.EXTRA_REFERRER, "a");
        } else {
            intent = new Intent(this, HomeActivity.class);
        }
        startActivity(intent);
        finish();
        if (navigateFromSignIn) { // Track GA with "Login" event
            App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                    AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                    AppConstant.GA_TRACKING_LABEL.SIGN_IN.getValue());
        }
    }

    @Override
    public void onCheckPassCodeFailure() {
        checkPassCodeError();
    }

    @Override
    public void onCheckPassCodeNone() {
        checkPassCodeError();
    }

    private void checkPassCodeError() {
        if (dialogHelper != null)
            dialogHelper.action(App.getInstance().getString(R.string.dialogTitleError), getString(R.string.dialogErrorNonePassCode), getString(R.string.text_ok), getString(R.string.text_cancel),
                    (dialogInterface, i) -> invokeError(),
                    (dialogInterface, i) -> toSignInScreen());
    }

    private void invokeError() {
        if (edtPasscode != null) {
            edtPasscode.postDelayed(() -> invokeSoftKey(edtPasscode), 100);
        }
    }

    private void toSignInScreen() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void onErrorGetToken() {
        if(dialogHelper != null){
            dialogHelper.showGetTokenError();
        }
    }

}
