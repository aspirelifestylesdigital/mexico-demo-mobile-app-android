package com.mastercard.mda.domain.mapper.profile;

import android.text.TextUtils;

import static com.mastercard.mda.domain.model.Profile.PHONE_MAX_LENGTH;

public final class MapDataApi {

    /*<string-array name="cate_salutations">
        <!--<item>Please select a salutation</item>-->
        <item>Dr</item>
        <item>Srita</item>
        <item>Sr</item>
        <item>Sra</item>
    </string-array>*/

    private static final String SALUTATION_MAP_DOCTOR = "Doctor";
    private static final String SALUTATION_DR = "Dr";

    private static final String SALUTATION_MISS_API = "Miss";
    private static final String SALUTATION_MISS = "Srita";


    private static final String SALUTATION_MR_API = "Mr.";
    private static final String SALUTATION_MRS_API_RES = "Mrs.";
    private static final String SALUTATION_MR = "Sr";

    private static final String SALUTATION_MS_API = "Ms";
    private static final String SALUTATION_MRS = "Sra";

    /*
    "enum":[
            "Captain",
            "Doctor",
            "Dr. (Miss)",
            "Dr. (Mr)",
            "Dr. (Mrs)",
            "Dr. (Ms)",
            "Fraulein",
            "Herr",
            "Madam",
            "Master",
            "Miss",
            "Mr.",
            "Mrs.",
            "Dr."
            "Ms",
            "Professor"
    ]*/
    /*
        <item>Dr.</item>
        <item>Dra.</item>
        <item>Srta.</item>
        <item>Sr.</item>
        <item>Sra.</item>
    */


    public static String mapSalutationRequest(String salutation) {
        if (TextUtils.isEmpty(salutation)) {
            return "";
        }

        String rs;

        if (SALUTATION_DR.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_MAP_DOCTOR;

        } else if (SALUTATION_MISS.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_MISS_API;

        } else if (SALUTATION_MR.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_MR_API;

        } else if (SALUTATION_MRS.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_MS_API;

        } else {

            rs = salutation;

        }

        return rs;
    }

    public static String mapSalutationResponse(String salutation) {
        if (TextUtils.isEmpty(salutation)) {
            return "";
        }

        String rs;

        if (SALUTATION_MAP_DOCTOR.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_DR;

        } else if (SALUTATION_MISS_API.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_MISS;

        } else if (SALUTATION_MR_API.equalsIgnoreCase(salutation)
                || SALUTATION_MRS_API_RES.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_MR;

        } else if (SALUTATION_MS_API.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_MRS;

        } else {

            rs = salutation;

        }

        return rs;
    }

    public static String mapPhoneResponse(String phone) {
        if (TextUtils.isEmpty(phone)) {
            return "";
        }

        phone = phone.replace("+", "");

        if (phone.length() > PHONE_MAX_LENGTH) {
            phone = phone.substring(0, PHONE_MAX_LENGTH);
        }

        return phone;
    }

    /**
     * force hard code difference value for each project <br><br>
     * field: homeCountry and location[].address.country with value: <br>
     * - BDA: "BRA"         <br>
     * - MDA: "MEX"         <br>
     * - default: "USA"     <br>
     * */
    static String getCountryCode(){
        return "MEX";
    }


}
