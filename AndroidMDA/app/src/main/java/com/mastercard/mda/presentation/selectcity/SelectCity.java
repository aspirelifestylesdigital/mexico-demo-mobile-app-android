package com.mastercard.mda.presentation.selectcity;

import com.mastercard.mda.presentation.base.BasePresenter;

/**
 * Created by tung.phan on 5/8/2017.
 */

public interface SelectCity {
    interface View {
    }

    interface Presenter extends BasePresenter<SelectCity.View> {
        void saveSelectCity(String cityName);
    }
}
