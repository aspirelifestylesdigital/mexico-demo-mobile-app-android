package com.mastercard.mda.presentation.checkout;

import android.content.Context;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.repository.AuthDataAspireRepository;
import com.api.aspire.domain.repository.AuthAspireRepository;
import com.api.aspire.domain.usecases.CheckPassCode;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SaveProfile;
import com.mastercard.mda.App;
import com.mastercard.mda.BuildConfig;
import com.mastercard.mda.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class PassCodeCheckoutPresenter implements PassCodeCheckout.Presenter {

    private CompositeDisposable compositeDisposable;
    private PassCodeCheckout.View view;
    private CheckPassCode checkPassCode;

    public PassCodeCheckoutPresenter(Context context) {
        this.compositeDisposable = new CompositeDisposable();
        AuthAspireRepository repository = new AuthDataAspireRepository();
        MapProfileApp mapProfileApp = new MapProfileApp();
        GetAccessToken getAccessToken = new GetAccessToken(context, mapProfileApp);
        LoadProfile loadProfile = new LoadProfile(context, mapProfileApp);
        SaveProfile saveProfile = new SaveProfile(context, mapProfileApp);
        GetToken getToken = new GetToken(context);

        this.checkPassCode = new CheckPassCode(
                context,
                repository,
                getAccessToken,
                loadProfile,
                saveProfile,
                getToken, BuildConfig.AS_TOKEN_USERNAME, BuildConfig.AS_TOKEN_SECRET);
    }

    @Override
    public void attach(PassCodeCheckout.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    private boolean isNoInternet() {
        boolean status;
        if (!App.getInstance().hasNetworkConnection()) {
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            status = true;
        } else {
            status = false;
        }
        return status;
    }

    @Override
    public void checkoutPossCode(String input) {
        if (isNoInternet()) {
            return;
        }

        view.showProgressDialog();
        compositeDisposable.add(checkPassCode.buildUseCaseSingle(
                new CheckPassCode.Params(
                        BuildConfig.AS_TOKEN_USERNAME,
                        BuildConfig.AS_TOKEN_SECRET,
                        input))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new PassCodeObserver()));
    }

    private final class PassCodeObserver extends DisposableSingleObserver<Boolean> {

        @Override
        public void onSuccess(Boolean aBoolean) {
            if (view == null) return;
            view.dismissProgressDialog();
            if (aBoolean != null && aBoolean)
                view.onCheckPassCodeSuccessfully();
            else
                view.onCheckPassCodeFailure();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view == null) return;
            view.dismissProgressDialog();
            view.onCheckPassCodeFailure();
            dispose();
        }
    }

    @Override
    public void handleSaveUserPrefPassCode(String newPassCode) {
        if (isNoInternet()) {
            return;
        }
        view.showProgressDialog();

        compositeDisposable.add(
                checkPassCode.saveUserProfile(newPassCode)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {
                                view.dismissProgressDialog();
                                view.updatePassCodeApiCompleted();
                                dispose();
                            }

                            @Override
                            public void onError(Throwable e) {
                                if (view == null) return;
                                view.dismissProgressDialog();
                                if (ErrorApi.isGetTokenError(e.getMessage())) {
                                    view.onErrorGetToken();
                                } else {
                                    view.onCheckPassCodeFailure();
                                }
                                dispose();
                            }
                        })
        );
    }

    @Override
    public void handleSplashCheckPassCode() {
        if (isNoInternet()) {
            return;
        }
        view.showProgressDialog();
        compositeDisposable.add(checkPassCode.handleCheckPassCode()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SplashPassCodeObserver())
        );
    }

    private class SplashPassCodeObserver extends DisposableSingleObserver<CheckPassCode.ResultPassCode> {
        @Override
        public void onSuccess(CheckPassCode.ResultPassCode resultPassCode) {
            view.dismissProgressDialog();
            if (resultPassCode == null) {
                view.onCheckPassCodeFailure();
            } else {
                switch (resultPassCode.passCodeStatus) {
                    case VALID:
                        view.onCheckPassCodeSuccessfully();
                        break;
                    case INVALID:
                        view.onCheckPassCodeFailure();
                        break;
                    case NONE:
                    default:
                        view.onCheckPassCodeNone();
                        break;
                }
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view == null) return;

            view.dismissProgressDialog();
            if (ErrorApi.isGetTokenError(e.getMessage())) {
                view.onErrorGetToken();
            } else {
                view.onCheckPassCodeFailure();
            }
            dispose();
        }
    }
}
