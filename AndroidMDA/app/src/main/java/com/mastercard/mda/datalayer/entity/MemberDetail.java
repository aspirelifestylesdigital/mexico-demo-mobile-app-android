package com.mastercard.mda.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class MemberDetail {
    @Expose
    @SerializedName("VerificationCode")
    private String verificationCode;
    @Expose
    @SerializedName("OnlineMemberDetailId")
    private String onlineMemberDetailID;

    public MemberDetail() {
        this.verificationCode = "Deepa.SHANKAR@internationalsos.com";
    }

    public MemberDetail(String onlineMemberDetailID) {
        this.verificationCode = "Deepa.SHANKAR@internationalsos.com";
        this.onlineMemberDetailID = onlineMemberDetailID;
    }
}