package com.mastercard.mda.datalayer.entity.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 7/18/2017.
 */

public class ChangePasswordRequest {
    @Expose
    @SerializedName("ConsumerKey")
    private final String consumerKey;
    @Expose
    @SerializedName("Functionality")
    private final String functionality;
    @Expose
    @SerializedName("Email2")
    private final String email;
    @Expose
    @SerializedName("NewPassword")
    private final String newPassword;
    @Expose
    @SerializedName("OldPassword")
    private final String oldPassword;

    public ChangePasswordRequest(String email, String newPassword, String oldPassword) {
        this.consumerKey = "DMA";
        this.functionality = "ChangePassword";
        this.email = email;
        this.newPassword = newPassword;
        this.oldPassword = oldPassword;
    }
}
