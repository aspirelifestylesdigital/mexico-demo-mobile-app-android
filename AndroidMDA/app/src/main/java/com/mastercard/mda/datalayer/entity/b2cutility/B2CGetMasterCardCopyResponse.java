package com.mastercard.mda.datalayer.entity.b2cutility;

import com.google.gson.annotations.SerializedName;
import com.mastercard.mda.datalayer.entity.GetClientCopyResult;

/**
 * Created by vinh.trinh on 6/6/2017.
 */

public class B2CGetMasterCardCopyResponse {
    @SerializedName("GetClientCopyResult")
    private GetClientCopyResult result;

    public GetClientCopyResult getResult() {
        return result;
    }
}
