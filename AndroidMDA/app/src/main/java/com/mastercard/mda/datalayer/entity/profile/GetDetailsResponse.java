package com.mastercard.mda.datalayer.entity.profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class GetDetailsResponse {

    public final Boolean success;
    public final String message;
    public final String firstName;
    public final String lastName;
    public final String mobile;
    public final String email;
    public final String salutation;
    public final String onlineMemberDetailID;

    public GetDetailsResponse(String rawJson) throws JSONException {
        JSONObject json = new JSONObject(rawJson);
        JSONObject jsonMember = json.optJSONObject("Member");
        JSONArray memberDetails = json.optJSONArray("MemberDetails");
        if(jsonMember == null || memberDetails == null) {
            success = false;
            JSONArray messages = json.optJSONArray("message");
            if(null != messages) {
                message = messages.length() > 0 ? messages.getJSONObject(0).getString("message") : "";
            } else {
                message = json.getString("message");
            }
            firstName = null;
            lastName = null;
            mobile = null;
            email = null;
            salutation = null;
            onlineMemberDetailID = null;
        } else {
            success = true;
            message = null;
            firstName = jsonMember.getString("FIRSTNAME");
            lastName = jsonMember.getString("LASTNAME");
            mobile = jsonMember.getString("MOBILENUMBER");
            email = jsonMember.getString("EMAIL");
            salutation = jsonMember.getString("SALUTATION");
            onlineMemberDetailID = memberDetails.length() == 0 ? "" : memberDetails.getJSONObject(0)
                    .getString("ONLINEMEMBERDETAILID");
        }
    }
}