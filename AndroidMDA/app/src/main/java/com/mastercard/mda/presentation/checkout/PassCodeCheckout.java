package com.mastercard.mda.presentation.checkout;


import com.api.aspire.common.constant.ErrCode;
import com.mastercard.mda.presentation.base.BasePresenter;

public interface PassCodeCheckout {

    interface View {
        void showErrorDialog(ErrCode errCode, String extraMsg);

        void onErrorGetToken();

        void showProgressDialog();

        void dismissProgressDialog();

        void onCheckPassCodeSuccessfully();

        void onCheckPassCodeFailure();

        void onCheckPassCodeNone();

        void updatePassCodeApiCompleted();
    }

    interface Presenter extends BasePresenter<View> {
        void checkoutPossCode(String input);

        void handleSplashCheckPassCode();

        void handleSaveUserPrefPassCode(String newPassCode);
    }

}
