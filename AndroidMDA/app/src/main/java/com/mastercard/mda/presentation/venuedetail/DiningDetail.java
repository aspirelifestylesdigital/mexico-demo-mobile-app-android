package com.mastercard.mda.presentation.venuedetail;

import com.mastercard.mda.domain.model.explore.DiningDetailItem;
import com.mastercard.mda.presentation.base.BasePresenter;

/**
 * Created by ThuNguyen on 6/13/2017.
 */

public interface DiningDetail {

    interface View {
        void onGetDiningDetailFinished(DiningDetailItem diningDetailItem);
        void onUpdateFailed();
    }

    interface Presenter extends BasePresenter<View> {
        void getDiningDetail(Integer categoryId, Integer itemId);
    }

}
