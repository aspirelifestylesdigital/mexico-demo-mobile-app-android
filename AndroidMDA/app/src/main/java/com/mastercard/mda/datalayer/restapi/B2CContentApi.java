package com.mastercard.mda.datalayer.restapi;

import com.mastercard.mda.datalayer.entity.b2ccontent.B2CContentDetailResponse;
import com.mastercard.mda.datalayer.entity.b2ccontent.B2CContentFullRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * for all other categories business except Dining
 */
public interface B2CContentApi {

    @POST("conciergecontent/GetContentFull")
    Call<B2CContentDetailResponse> getContentFull(@Body B2CContentFullRequest body);
}
