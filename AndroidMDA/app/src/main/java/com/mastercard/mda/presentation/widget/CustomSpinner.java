package com.mastercard.mda.presentation.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.mastercard.mda.R;

/**
 * Created by anh.trinh on 8/8/2017.
 */

public class CustomSpinner {
    private TextView anchorView;
    private ListPopupWindow listPopupWindow;
    private Context context;
    private DropdownAdapter dropdownAdapter;

    public boolean isNone() {
        return isNone;
    }

    public void setNone(final boolean none) {
        isNone = none;
        this.dropdownAdapter.setNone(isNone);
        this.dropdownAdapter.notifyDataSetChanged();
    }

    boolean isNone = true;

    public int getSelectionPosition() {
        return selectionPosition;
    }

    public void setSelectionPosition(final int selectionPosition) {
        this.selectionPosition = selectionPosition;
        this.listPopupWindow.setSelection(this.selectionPosition);
        this.dropdownAdapter.setCurrentSelected(selectionPosition);
        if (selectionPosition == 0 && isNone) {
            anchorView.setTextColor(Color.parseColor("#99A1A8"));
        } else {
            anchorView.setTextColor(Color.parseColor("#011627"));
        }
        anchorView.setText(dropdownAdapter.getItem(selectionPosition)
                                          .toString());
    }

    private int selectionPosition;

    public interface SpinnerListener {
        void onArchorViewClick();

        void onItemSelected(int index);
    }

    public SpinnerListener getmSpinnerListener() {
        return mSpinnerListener;
    }

    public void setmSpinnerListener(final SpinnerListener mSpinnerListener) {
        this.mSpinnerListener = mSpinnerListener;
    }

    SpinnerListener mSpinnerListener;

    public CustomSpinner(Context context,
                         final TextView anchorView, boolean isNone) {
        this.context = context;
        this.anchorView = anchorView;
        this.isNone = isNone;
        listPopupWindow = new ListPopupWindow(anchorView.getContext());
        anchorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (mSpinnerListener != null) {
                    mSpinnerListener.onArchorViewClick();
                }
                if(listPopupWindow.isShowing()){
                    listPopupWindow.dismiss();
                    anchorView.setBackgroundResource(R.drawable.bg_spinner_hide);
                }else {
                    listPopupWindow.show();
                    anchorView.setBackgroundResource(R.drawable.bg_spinner_show);
                }
            }
        });

        listPopupWindow.setAnchorView(anchorView);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                anchorView.setBackgroundResource(R.drawable.bg_spinner_hide);
            }
        });
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView,
                                    final View view,
                                    final int i,
                                    final long l) {
                if (mSpinnerListener != null) {
                    mSpinnerListener.onItemSelected(i);
                }
                if (i == 0 &&isNone) {
                    anchorView.setTextColor(Color.parseColor("#99A1A8"));
                } else {
                    anchorView.setTextColor(Color.parseColor("#011627"));
                }
                anchorView.setText(adapterView.getItemAtPosition(i)
                                              .toString());
                selectionPosition = i;

                dropdownAdapter.setCurrentSelected(i);
                listPopupWindow.dismiss();

            }
        });
    }
    public CustomSpinner(Context context,
                         final TextView anchorView) {
        this.context = context;
        this.anchorView = anchorView;
        listPopupWindow = new ListPopupWindow(anchorView.getContext());
        anchorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (mSpinnerListener != null) {
                    mSpinnerListener.onArchorViewClick();
                }
                if(listPopupWindow.isShowing()){
                    listPopupWindow.dismiss();
                    anchorView.setBackgroundResource(R.drawable.bg_spinner_hide);
                }else {
                    listPopupWindow.show();
                    anchorView.setBackgroundResource(R.drawable.bg_spinner_show);
                }
            }
        });
        listPopupWindow.setModal(true);
        listPopupWindow.setAnchorView(anchorView);
        listPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                anchorView.setBackgroundResource(R.drawable.bg_spinner_hide);
            }
        });
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView,
                                    final View view,
                                    final int i,
                                    final long l) {
                if (mSpinnerListener != null) {
                    mSpinnerListener.onItemSelected(i);
                }
                if (i == 0 &&isNone) {
                    anchorView.setTextColor(Color.parseColor("#99A1A8"));
                } else {
                    anchorView.setTextColor(Color.parseColor("#011627"));
                }
                anchorView.setText(adapterView.getItemAtPosition(i)
                                              .toString());
                selectionPosition = i;
                dropdownAdapter.setCurrentSelected(i);
                listPopupWindow.dismiss();

            }
        });
    }

    public static int convertDpToPx(int dp,
                                    Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                                               dp,
                                               context.getResources()
                                                      .getDisplayMetrics());
    }

    public void setPobpupHeight(int popupHeight) {
        listPopupWindow.setHeight(convertDpToPx(popupHeight,
                                                context));
    }


    public DropdownAdapter getDropdownAdapter() {
        return dropdownAdapter;
    }

    public void setDropdownAdapter(final DropdownAdapter dropdownAdapter) {
        this.dropdownAdapter = dropdownAdapter;
        this.dropdownAdapter.setNone(isNone);
        listPopupWindow.setAdapter(dropdownAdapter);
    }


}
