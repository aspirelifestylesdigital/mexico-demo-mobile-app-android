package com.mastercard.mda.presentation.checkout;


import android.content.Context;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.domain.usecases.SignInCase;
import com.mastercard.mda.App;
import com.mastercard.mda.R;
import com.mastercard.mda.domain.usecases.GetUserApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class SignInPresenter implements SignIn.Presenter {

    private CompositeDisposable compositeDisposable;
    private SignIn.View view;
    private GetUserApp getUserApp;

    SignInPresenter(Context c) {
        compositeDisposable = new CompositeDisposable();
        this.getUserApp = new GetUserApp(c);
    }

    @Override
    public void attach(SignIn.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void doSignIn(String email, String password) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        compositeDisposable.add(
                getUserApp.signIn(new SignInCase.Params(email, password))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {
                                if (view == null) return;
                                view.dismissProgressDialog();
                                view.proceedToHome();
                                dispose();
                            }

                            @Override
                            public void onError(Throwable e) {
                                if (view == null) return;
                                view.dismissProgressDialog();
                                if (ErrCode.PASS_CODE_ERROR.name().equals(e.getMessage())
                                        || ErrCode.PASS_CODE_INVALID_ERROR.name().equals(e.getMessage())) {
                                    view.onCheckPassCodeFailure();
                                } else if (ErrorApi.isGetTokenError(e.getMessage())) {
                                    view.showErrorDialog(ErrCode.API_ERROR, App.getInstance().getString(R.string.errorSignIn));
                                } /*else if (e instanceof BackendException) {
                                    view.showErrorDialog(ErrCode.API_ERROR, e.getMessage());
                                } */ else {
                                    view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
                                }
                                dispose();
                            }
                        })
        );

    }

    @Override
    public void abort() {
        compositeDisposable.clear();
    }
}
