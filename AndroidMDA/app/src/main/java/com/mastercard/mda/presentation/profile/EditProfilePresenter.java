package com.mastercard.mda.presentation.profile;

import android.content.Context;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.exception.BackendException;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SaveProfile;
import com.mastercard.mda.App;
import com.mastercard.mda.BuildConfig;
import com.mastercard.mda.domain.mapper.profile.MapProfile;
import com.mastercard.mda.domain.model.Profile;
import com.mastercard.mda.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public class EditProfilePresenter implements MyProfile.Presenter {
    private CompositeDisposable disposables;
    private LoadProfile loadProfile;
    private SaveProfile saveProfile;
    private GetAccessToken getAccessToken;
    private MyProfile.View view;

    private ProfileAspire profileCache;
    private MapProfile mapProfile;

    EditProfilePresenter(Context context) {
        disposables = new CompositeDisposable();
        MapProfileApp mapProfileApp = new MapProfileApp();
        this.loadProfile = new LoadProfile(context, mapProfileApp);
        this.saveProfile = new SaveProfile(context, mapProfileApp);
        this.getAccessToken = new GetAccessToken(context, mapProfileApp);
        this.mapProfile = new MapProfile();
    }

    @Override
    public void attach(MyProfile.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        view = null;
    }

    @Override
    public void getProfile() {
        view.showProgressDialog();
        disposables.add(
                loadProfile.loadRemote(getAccessToken)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new LoadProfileObserver())
        );
    }

    public void getProfileLocal(){
        disposables.add(
                loadProfile.loadStorage()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new LoadProfileLocalObserver())
        );
    }

    @Override
    public void updateProfile(Profile profile) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        if (profileCache == null) {
            //not happen here
            return;
        }
        view.showProgressDialog();
        ProfileAspire profileRequest = mapProfile.updateProfile(profileCache, profile);
        disposables.add(saveProfile.updateProfile(profileRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SaveProfileObserver()));
    }

    @Override
    public void abort() {
        disposables.clear();
    }

    private class LoadProfileObserver extends DisposableSingleObserver<ProfileAspire> {

        @Override
        public void onSuccess(@NonNull ProfileAspire profile) {
            loadProfileOnUI(profile);
        }

        protected void loadProfileOnUI(ProfileAspire profile) {
            if (view != null) {
                profileCache = profile;
                view.showProfile(mapProfile.getProfile(profile), true);
                view.dismissProgressDialog();
            }
            dispose();
        }

        @Override
        public void onError(@NonNull Throwable e) {
            if (view != null) {
                view.dismissProgressDialog();
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            }
            dispose();
        }
    }

    private final class LoadProfileLocalObserver extends LoadProfileObserver {

        @Override
        public void onSuccess(ProfileAspire profile) {
            super.onSuccess(profile);
        }

        @Override
        protected void loadProfileOnUI(ProfileAspire profile) {
            profileCache = profile;
            view.showProfile(mapProfile.getProfile(profile), false);
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }
    }

    private final class SaveProfileObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            if (view == null) return;

            view.profileUpdated();
            view.dismissProgressDialog();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view == null) return;

            view.dismissProgressDialog();
            if (e instanceof BackendException) {
                view.showErrorDialog(ErrCode.API_ERROR, e.getMessage());
            } else {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            }
            dispose();
        }
    }


}
