package com.mastercard.mda.datalayer.restapi;

import com.mastercard.mda.BuildConfig;
import com.mastercard.mda.datalayer.entity.oauth.AccessTokenResponse;
import com.mastercard.mda.datalayer.entity.oauth.RefreshTokenResponse;
import com.mastercard.mda.datalayer.entity.oauth.ReqTokenResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by vinh.trinh on 6/19/2017.
 */

public interface OAuthApi {
    String CALLBACK_URL = "urn:ietf:wg:oauth:2.0:oob";

    @Headers({BuildConfig.WS_ConciergeOauth_ConsumerKey, BuildConfig.WS_ConciergeOauth_ConsumerSecret})
    @GET("OAuth/AuthoriseRequestToken?CallbackURL="+CALLBACK_URL)
    Call<ReqTokenResponse> requestToken(@Query("MemberDeviceId") String deviceID, @Query("OnlineMemberId") String onlineMemberId);

    @GET("OAuth/GetAccessToken?CallbackURL="+CALLBACK_URL+"&ConsumerKey=DMA")
    Call<AccessTokenResponse> accessToken(@Query("RequestToken") String requestToken, @Query("OnlineMemberId") String onlineMemberId);

    @GET("OAuth/RefereshToken")
    Call<RefreshTokenResponse> refreshToken(@Query("RequestToken") String requestToken, @Query("AccessToken") String accessToken);
}