package com.mastercard.mda.presentation.checkout;

import com.mastercard.mda.presentation.base.BasePresenter;
import com.api.aspire.common.constant.ErrCode;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public interface SignIn {

    interface View {
        void proceedToHome();
        void onCheckPassCodeFailure();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void doSignIn(String email, String password);
        void abort();
    }

}
