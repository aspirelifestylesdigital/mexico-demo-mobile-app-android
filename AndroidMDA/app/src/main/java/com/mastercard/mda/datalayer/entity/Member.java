package com.mastercard.mda.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class Member {
    @Expose
    @SerializedName("ConsumerKey")
    private String consumerKey;
    @Expose
    @SerializedName("Functionality")
    private String functionality;
    @Expose
    @SerializedName("Email")
    private String email;
    @Expose
    @SerializedName("FirstName")
    private String firstName;
    @Expose
    @SerializedName("LastName")
    private String lastName;
    @Expose
    @SerializedName("MobileNumber")
    private String phoneNumber;
    @Expose
    @SerializedName("Password")
    private String secretKey;
    @Expose
    @SerializedName("Salutation")
    private String salutation;
    @Expose
    @SerializedName("MemberDeviceID")
    private String memberDeviceID;
    @Expose
    @SerializedName("onlineMemberID")
    private String onlineMemberID;

    public Member(String email, String firstName, String lastName,
                  String phoneNumber, String password, String salutation, String memberDeviceID) {
        this.consumerKey = "DMA";
        this.functionality = "Registration";
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.secretKey = password;
        this.salutation = salutation;
        this.memberDeviceID = memberDeviceID;
    }

    public Member(String email, String firstName, String lastName, String phoneNumber,
                  String password, String salutation, String memberDeviceID, String onlineMemberID) {
        this.consumerKey = "DMA";
        this.functionality = "Registration";
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.secretKey = password;
        this.salutation = salutation;
        this.memberDeviceID = memberDeviceID;
        this.onlineMemberID = onlineMemberID;
    }
}