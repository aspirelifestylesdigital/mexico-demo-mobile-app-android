package com.mastercard.mda.datalayer.datasource;

import com.mastercard.mda.datalayer.entity.geocoder.GeoCoderResponse;
import com.mastercard.mda.datalayer.restapi.GeoCoderApi;
import com.mastercard.mda.datalayer.retro2client.AppHttpClient;
import com.mastercard.mda.domain.model.LatLng;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vinh.trinh on 7/31/2017.
 */

public class AppGeoCoder {

    public String getAddress(LatLng location) {
        GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
        Call<GeoCoderResponse> request = geoCoderApi.getAddress(location.toString());
        try {
            Response<GeoCoderResponse> response = request.execute();
            GeoCoderResponse responseBody = response.body();
            if(response.isSuccessful() && responseBody.success()) {
                GeoCoderResponse.AddressComponent[] addressComponents = responseBody.addressComponents();
                StringBuilder stringBuilder = new StringBuilder(addressComponents[0].name());
                for (int i = 1; i < addressComponents.length; i++) {
                    stringBuilder.append(", ").append(addressComponents[i].name());
                }
                return stringBuilder.toString();
            } else {
                return "";
            }
        } catch (IOException e) {
            return "";
        }
    }
    public String getFormattedAddress(LatLng location) {
        GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
        Call<GeoCoderResponse> request = geoCoderApi.getAddress(location.toString());
        try {
            Response<GeoCoderResponse> response = request.execute();
            GeoCoderResponse responseBody = response.body();
            if(response.isSuccessful() && responseBody.success()) {
                return responseBody.getFormattedAddress();
            } else {
                return "";
            }
        } catch (IOException e) {
            return "";
        }
    }
    public LatLng getLocation(String strAddress) {
        GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
        Call<GeoCoderResponse> request = geoCoderApi.getLatLng(strAddress);
        try {
            Response<GeoCoderResponse> response = request.execute();
            GeoCoderResponse responseBody = response.body();
            if(response.isSuccessful() && responseBody.success()) {
                return new LatLng(responseBody.location().getLat(), responseBody.location().getLng());
            } else {
                return LatLng.INVALID;
            }
        } catch (IOException e) {
            return LatLng.INVALID;
        }
    }
    public GeoCoderResponse getFullGeoCoder(String strAddress){
        GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
        Call<GeoCoderResponse> request = geoCoderApi.getLatLng(strAddress);
        try {
            Response<GeoCoderResponse> response = request.execute();
            GeoCoderResponse responseBody = response.body();
            if(response.isSuccessful() && responseBody.success()) {
                return responseBody;
            }
        } catch (IOException e) {
            return null;
        }
        return null;
    }
}
