package com.mastercard.mda.domain.usecases;

import android.content.Context;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.repository.AuthDataAspireRepository;
import com.api.aspire.domain.repository.AuthAspireRepository;
import com.api.aspire.domain.usecases.CheckPassCode;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SaveProfile;
import com.api.aspire.domain.usecases.SignInCase;
import com.mastercard.mda.BuildConfig;

import io.reactivex.Completable;

public class GetUserApp {

    private SignInCase signInCase;
    private CheckPassCode checkPassCode;

    public GetUserApp(Context context) {
        MapProfileApp mapProfileApp = new MapProfileApp();
        signInCase = new SignInCase(context, mapProfileApp);

        AuthAspireRepository repository = new AuthDataAspireRepository();
        GetAccessToken getAccessToken = new GetAccessToken(context, mapProfileApp);
        LoadProfile loadProfile = new LoadProfile(context, mapProfileApp);
        SaveProfile saveProfile = new SaveProfile(context, mapProfileApp);
        GetToken getToken = new GetToken(context);

        checkPassCode = new CheckPassCode(
                context,
                repository,
                getAccessToken,
                loadProfile,
                saveProfile,
                getToken, BuildConfig.AS_TOKEN_USERNAME, BuildConfig.AS_TOKEN_SECRET);
    }

    public Completable signIn(SignInCase.Params params) {
        return signInCase.getProfileNoSaveStorage(params)
                .flatMapCompletable(profileAspire ->

                        checkPassCode.handleCheckPassCode(profileAspire)
                                .flatMapCompletable(resultPassCode -> {
                                    if (CheckPassCode.PassCodeStatus.VALID == resultPassCode.getPassCodeStatus()) {

                                        return signInCase.saveProfileStoreage(profileAspire)
                                                .flatMapCompletable(profileAspireLocal -> Completable.complete());

                                    } else {
                                        return Completable.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
                                    }
                                }));
    }
}