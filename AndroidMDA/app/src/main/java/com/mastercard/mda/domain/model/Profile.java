package com.mastercard.mda.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vinh.trinh on 4/27/2017.
 */
public class Profile implements Parcelable {

    public static final int PHONE_MAX_LENGTH = 20;
    public static final int PHONE_LENGTH_ACCEPT = PHONE_MAX_LENGTH + 1; //-- default 21
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String salutation;
    private boolean locationOn;

    public boolean anyIsEmpty() {
        return firstName.length() == 0 || lastName.length() == 0 || email.length() == 0 ||
                phone.length() == 0 ;
    }

    public boolean allIsEmpty() {
        return firstName.length() == 0 && lastName.length() == 0 && email.length() == 0 &&
                phone.length() == 0;
    }

    public boolean anyEmpty(EditText editTextPwd, EditText editeTextConfirmPwd, EditText editTextPassCode) {
        return firstName.length() == 0 || lastName.length() == 0 || email.length() == 0 ||
                phone.length() == 0 || editTextPwd.getText().length() == 0 || editeTextConfirmPwd.getText().length() == 0
                || editTextPassCode.length() == 0 || allIsEmpty();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        if(phone != null){
            phone = phone.replace("+", "");
            if(phone.length() > PHONE_MAX_LENGTH){
                phone = phone.substring(0, PHONE_MAX_LENGTH);
            }
        }
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDisplayCountryCodeAndPhone(){
        if(phone == null){
            return "";
        }
        String valuePhone = phone.replace("+", "");
        if(valuePhone.length() >= PHONE_MAX_LENGTH){
            //don't do anything => hold original value phone
        }else {
            valuePhone = "+" + valuePhone;
        }
        return valuePhone;
    }

    public boolean isLocationOn() {
        return locationOn;
    }

    public void locationToggle(boolean on) {
        this.locationOn = on;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public Profile() {
        String temp = "";
        firstName = temp;
        lastName = temp;
        email = temp;
        phone = temp;
        salutation = temp;
    }

    public Profile(String jsonString) throws JSONException {
        JSONObject json = new JSONObject(jsonString);
        this.firstName = json.getString("firstName");
        this.lastName = json.getString("lastName");
        this.email = json.getString("email");
        this.phone = json.getString("phone");
        this.salutation = json.getString("salutation");
        this.locationOn = json.optBoolean("locationToggle");
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("firstName", firstName);
        json.put("lastName", lastName);
        json.put("email", email);
        json.put("phone", phone);
        json.put("salutation", salutation);
        json.put("locationToggle", locationOn);
        return json;
    }

    private Profile(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        phone = in.readString();
        locationOn = in.readInt() == 1;
    }
    public void addPlusSignAtTheFirstIfAny(){
        if(phone != null && !phone.startsWith("+")){
            phone = "+" + phone;
        }
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeInt(locationOn ? 1 : 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
