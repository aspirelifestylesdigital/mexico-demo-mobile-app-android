package com.mastercard.mda.presentation.venuedetail;

import com.mastercard.mda.domain.model.explore.OtherExploreDetailItem;
import com.mastercard.mda.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface OtherExploreDetail {

    interface View {
        void onGetContentFullFinished(OtherExploreDetailItem exploreRViewItem);
        void onUpdateFailed();
        void noInternetMessage();
    }

    interface Presenter extends BasePresenter<View> {
        void getContent(Integer contentId);
    }

}
