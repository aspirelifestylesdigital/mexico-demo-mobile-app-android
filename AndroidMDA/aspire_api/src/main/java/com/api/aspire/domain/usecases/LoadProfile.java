package com.api.aspire.domain.usecases;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class LoadProfile extends UseCase<ProfileAspire, String> {

    private PreferencesStorageAspire preferencesStorage;
    private ProfileAspireRepository profileRepository;

    public LoadProfile(Context context, MapProfileBase mapLogic) {

        this.preferencesStorage = new PreferencesStorageAspire(context);
        this.profileRepository = new ProfileDataAspireRepository(preferencesStorage, mapLogic);

    }

    @Override
    Observable<ProfileAspire> buildUseCaseObservable(String accessToken) {
        return null;
    }

    @Override
    public Single<ProfileAspire> buildUseCaseSingle(String accessToken) {
        if (TextUtils.isEmpty(accessToken)) {
            return loadStorage();
        } else {
            return profileRepository.loadProfile(accessToken);
        }
    }

    public Single<ProfileAspire> loadStorage() {
        return profileRepository.loadProfile(null);
    }

    public Single<ProfileAspire> loadRemote(GetAccessToken getAccessToken) {
        return getAccessToken.executeParams()
                .flatMap(params -> profileRepository.loadProfile(params.getAccessToken()));
    }

    @Override
    Completable buildUseCaseCompletable(String accessToken) {
        return null;
    }

}