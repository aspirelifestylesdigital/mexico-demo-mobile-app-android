package com.api.aspire.data.restapi;

import com.api.aspire.BuildConfig;
import com.api.aspire.data.entity.oauth.GetTokenAspireResponse;
import com.api.aspire.data.entity.passcode.PassCodeRequest;
import com.api.aspire.data.entity.passcode.PassCodeResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface AspireOAuthApi {

    @Headers({BuildConfig.WS_ASPIRE_TOKEN_AUTHORIZATION})
    @FormUrlEncoded
    @POST(".")
    Call<GetTokenAspireResponse> getTokenService(@Field("grant_type") String grant_type,
                                                 @Field("redirect_uri") String redirect_uri,
                                                 @Field("scope") String scope,
                                                 @Field("username") String username,
                                                 @Field("password") String password
    );

    @Headers({BuildConfig.WS_ASPIRE_TOKEN_AUTHORIZATION})
    @FormUrlEncoded
    @POST(".")
    Call<GetTokenAspireResponse> getTokenUser(@Field("grant_type") String grant_type,
                                              @Field("redirect_uri") String redirect_uri,
                                              @Field("scope") String scope,
                                              @Field("username") String username,
                                              @Field("password") String password
    );
}