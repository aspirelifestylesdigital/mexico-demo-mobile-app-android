package com.api.aspire.domain.usecases;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.data.entity.preference.PreferenceData;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableSource;
import io.reactivex.Observable;
import io.reactivex.Single;

public class SignInCase extends UseCase<Void, SignInCase.Params> {

    private GetToken getToken;
    private ProfileAspireRepository profileRepository;

    //<editor-fold desc="Variable">
    private String passCodeStorageCache;
    private String accessTokenCache;
    //</editor-fold>

    public SignInCase(Context context, MapProfileBase mapLogic) {
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(context);

        this.profileRepository = new ProfileDataAspireRepository(preferencesStorage, mapLogic);
        this.getToken = new GetToken(context);

    }

    @Override
    Observable<Void> buildUseCaseObservable(Params aVoid) {
        return null;
    }


    @Override
    Single<Void> buildUseCaseSingle(Params params) {
        return null;
    }

    @Override
    public Completable buildUseCaseCompletable(Params params) {
//        return checkPassCode.passCodeStorage()
//                .zipWith(getProfile(params),
//                        (passCode, profile) -> {
//                            passCodeStorageCache = passCode;
//                            return profile;
//                        })
//                .flatMapCompletable(this::handleSavePassCode);
        return null;
    }


    public Completable handleSavePassCode(ProfileAspire profile, String passCodeStorageCache, int bin) {
        if (profile == null || profile.getPreferenceData() == null
                || TextUtils.isEmpty(passCodeStorageCache)
                || TextUtils.isEmpty(accessTokenCache)) {

            return Completable.error(new Exception("Error General save PassCode"));
        }

        PreferenceData preferenceData = profile.getPreferenceData();

        String passCodeRemote = preferenceData.getPassCode();

        Completable rsCompletable;

        if (TextUtils.isEmpty(passCodeRemote)
                || !passCodeRemote.equals(passCodeStorageCache)) {

            preferenceData.setPassCodeValue(passCodeStorageCache);
            profile.setPreferenceData(preferenceData);

            rsCompletable = profileRepository.saveProfile(accessTokenCache, profile);

        } else {

            rsCompletable = Completable.create(CompletableEmitter::onComplete);
        }

        return rsCompletable;
    }

    public Single<ProfileAspire> getProfile(Params params) {
        return getToken.getTokenUser(params.email, params.password)
                .flatMap(accessToken ->{
                    accessTokenCache = accessToken;
                    return profileRepository.signInProfile(accessToken, params.password, params.email);
                });
    }

    //<editor-fold desc="flow check passCode Sign In project MDA, BDA ">

    public Single<ProfileAspire> getProfileNoSaveStorage(Params params) {
        return getToken.getTokenUser(params.email, params.password)
                .flatMap(accessToken ->{
                    accessTokenCache = accessToken;
                    return profileRepository.signInNoSaveStorage(accessToken, params.password, params.email);
                });
    }

    public Single<ProfileAspire> saveProfileStoreage(ProfileAspire profileAspire) {
        return profileRepository.saveProfileStorage(profileAspire);
    }

    //</editor-fold>

    public static class Params {
        final String email;
        final String password;

        public Params(String email, String password) {
            this.email = email;
            this.password = password;
        }
    }
}
