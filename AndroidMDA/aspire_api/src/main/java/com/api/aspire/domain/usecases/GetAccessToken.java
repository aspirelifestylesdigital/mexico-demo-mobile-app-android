package com.api.aspire.domain.usecases;


import android.content.Context;

import io.reactivex.Single;

public class GetAccessToken {

    private LoadProfile loadProfile;
    private GetToken getToken;


    public GetAccessToken(Context context, MapProfileBase mapLogic) {
        this.loadProfile = new LoadProfile(context, mapLogic);
        this.getToken = new GetToken(context);
    }

    public Single<String> getTokenSignIn() {
        return loadProfile.loadStorage()
                .flatMap(profile -> getToken.getTokenUser(profile.getEmail(), profile.getSecretKeyDecrypt()));
    }

    /**
     * get Access Token
     */
    public Single<String> execute() {
        return loadProfile.loadStorage()
                .flatMap(profile -> getToken.refreshToken(profile.getEmail(), profile.getSecretKeyDecrypt()));
    }

    public Single<Params> executeParams() {
        return loadProfile.loadStorage()
                .flatMap(profile -> {
                    String secretKey = profile.getSecretKeyDecrypt();
                    return getToken.refreshToken(profile.getEmail(), profile.getSecretKeyDecrypt()).zipWith(Single.just(secretKey),
                            Params::new);
                });
    }

    public static final class Params {
        final String accessToken;
        /* value pass decrypt */
        final String secretKeyDecrypt;

        public Params(String accessToken, String secretKeyDecrypt) {
            this.accessToken = accessToken;
            this.secretKeyDecrypt = secretKeyDecrypt;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public String getSecretKeyDecrypt() {
            return secretKeyDecrypt;
        }
    }
}