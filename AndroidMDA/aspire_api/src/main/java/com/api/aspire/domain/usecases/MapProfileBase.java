package com.api.aspire.domain.usecases;

import com.api.aspire.data.entity.profile.UpdateProfileAspireRequest;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;

import io.reactivex.Single;

public interface MapProfileBase {

    /**
     * @param secretKeyCurrent: deEncrypt pass
     */
    Single<ProfileAspire> mapResponse(PreferencesStorageAspire preferencesStorage,
                                      final ProfileAspire profileAspire,
                                      String secretKeyCurrent,
                                      String userEmailCurrent);

    UpdateProfileAspireRequest mapUpdate(ProfileAspire profile, String passCodeLocal);

    String mapSalutation(String profileSalutation);

}
