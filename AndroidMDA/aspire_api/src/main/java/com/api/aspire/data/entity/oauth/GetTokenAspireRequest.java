package com.api.aspire.data.entity.oauth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 7/18/2017.
 */

public class GetTokenAspireRequest {
    @Expose
    @SerializedName("grant_type")
    private final String grantType;
    @Expose
    @SerializedName("redirect_uri")
    private final String redirectUri;
    @Expose
    @SerializedName("scope")
    private final String scope;
    @Expose
    @SerializedName("username")
    private final String username;
    @Expose
    @SerializedName("password")
    private final String secretKey;

    public GetTokenAspireRequest(String username, String secretKey) {
        this.grantType = "password";
        this.redirectUri = "http://www.google.com";
        this.scope = "openid";
        this.username = username;
        this.secretKey = secretKey;
    }

    /**
     * get token service
     */
    /*public GetTokenAspireRequest() {
        this("dma-qa@aspire.org", "P@ssw0rd123");
    }*/

    public String getGrantType() {
        return grantType;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public String getScope() {
        return scope;
    }

    public String getUsername() {
        return username;
    }

    public String getSecretKey() {
        return secretKey;
    }
}
