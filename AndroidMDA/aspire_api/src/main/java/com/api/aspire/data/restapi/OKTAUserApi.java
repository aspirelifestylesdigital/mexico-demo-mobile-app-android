package com.api.aspire.data.restapi;


import com.api.aspire.BuildConfig;
import com.api.aspire.data.entity.profile.ChangePasswordOKTARequest;
import com.api.aspire.data.entity.profile.ChangePasswordOKTAResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface OKTAUserApi {

    @Headers({"Authorization: " + BuildConfig.WS_OKTA_TOKEN_AUTHORIZATION, "Content-Type: application/json"})
    @POST("{email}/credentials/change_password")
    Call<ChangePasswordOKTAResponse> changePassword(@Path("email") String email,
                                                    @Body ChangePasswordOKTARequest body);

    @Headers({"Authorization: " + BuildConfig.WS_OKTA_TOKEN_AUTHORIZATION, "Content-Type: application/json"})
    @POST("{email}/lifecycle/reset_password?sendEmail=true")
    Call<ResponseBody> forgotPassword(@Path("email") String email);

}