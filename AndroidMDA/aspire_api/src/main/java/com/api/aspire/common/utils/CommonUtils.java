package com.api.aspire.common.utils;

import android.text.TextUtils;

public class CommonUtils {

    /** return -1 can not parse String to int */
    public static int convertStringToInt(String str) {
        if(TextUtils.isEmpty(str)){
            return -1;
        }

        int number = -1;
        try {
            number = Integer.valueOf(str);
        } catch (Exception ignored) {
        }

        return number;
    }

}
