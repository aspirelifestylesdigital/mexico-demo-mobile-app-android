package com.api.aspire.data.restapi;


import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.data.entity.profile.UpdateProfileAspireRequest;
import com.api.aspire.data.entity.profile.UpdateProfileAspireResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PUT;

public interface AspireProfileApi {

    String WS_API_PATH_PROFILE = "profiles/";

    @Headers({"Content-Type: application/json"})
    @GET(WS_API_PATH_PROFILE)
    Call<ProfileAspireResponse> retrieveProfile(@Header("Authorization") String auth);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @PUT(WS_API_PATH_PROFILE)
    Call<UpdateProfileAspireResponse> updateProfile(@Header("Authorization") String auth,
                                                    @Body UpdateProfileAspireRequest profileResponse);
}
