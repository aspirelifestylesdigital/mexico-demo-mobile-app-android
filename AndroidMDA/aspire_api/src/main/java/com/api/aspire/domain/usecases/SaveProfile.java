package com.api.aspire.domain.usecases;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class SaveProfile extends UseCase<Void, SaveProfile.Params> {
    private ProfileAspireRepository profileRepository;
    private GetAccessToken mGetAccessToken;
    private LoadProfile mLoadProfile;

    private boolean shouldGetProfile;

    public SaveProfile(Context c, MapProfileBase mapLogic) {
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(c);
        this.profileRepository = new ProfileDataAspireRepository(preferencesStorage, mapLogic);
        this.mGetAccessToken = new GetAccessToken(c, mapLogic);
        this.mLoadProfile = new LoadProfile(c, mapLogic);
    }

    @Override
    Observable<Void> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(Params params) {
        return null;
    }

    @Override
    public Completable buildUseCaseCompletable(Params params) {
        return profileRepository.saveProfile(params.accessToken, params.profile);
    }

    public Completable saveProfileSelectCity(ProfileAspire profileAspire) {
        shouldGetProfile = true;

        if (profileAspire != null && profileAspire.getAppUserPreferences() != null) {

            for (ProfileAspireResponse.AppUserPreferences preference : profileAspire.getAppUserPreferences()) {
                if (ConstantAspireApi.APP_USER_PREFERENCES.SELECTED_CITY_KEY.equals(preference.getPreferenceKey())
                        && !TextUtils.isEmpty(preference.getAppUserPreferenceId())) {
                    shouldGetProfile = false;
                }
            }
        }
        return mGetAccessToken.execute()
                .flatMapCompletable(accessToken -> {
                    if (shouldGetProfile) {
                        return buildUseCaseCompletable(new Params(profileAspire, accessToken))
                                .andThen(mLoadProfile.buildUseCaseSingle(accessToken)
                                        .flatMapCompletable(pro -> Completable.complete()));
                    } else {
                        return buildUseCaseCompletable(new Params(profileAspire, accessToken));
                    }
                });
    }

    public Completable updateProfile(ProfileAspire profile) {
        return mGetAccessToken.execute().flatMapCompletable(
                accessToken -> buildUseCaseCompletable(new Params(profile, accessToken)));
    }


    public static class Params {
        private ProfileAspire profile;
        private String accessToken;

        public Params(ProfileAspire profile, String accessToken) {
            this.profile = profile;
            this.accessToken = accessToken;
        }
    }
}
