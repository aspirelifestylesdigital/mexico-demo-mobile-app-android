package com.api.aspire.data.entity.profile;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordOKTARequest {

    @SerializedName("oldPassword")
    private OldPassword oldSecret;

    @SerializedName("newPassword")
    private NewPassword newSecret;

    public ChangePasswordOKTARequest(String oldSecret, String newSecret) {
        this.oldSecret = new OldPassword(oldSecret);
        this.newSecret = new NewPassword(newSecret);
    }

    class NewPassword {

        @SerializedName("value")
        private String value;

        public NewPassword(String value) {
            this.value = value;
        }
    }

    class OldPassword {

        @SerializedName("value")
        private String value;

        public OldPassword(String value) {
            this.value = value;
        }
    }
}