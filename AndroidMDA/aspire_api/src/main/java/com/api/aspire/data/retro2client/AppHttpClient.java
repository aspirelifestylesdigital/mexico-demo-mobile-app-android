package com.api.aspire.data.retro2client;

import com.api.aspire.BuildConfig;
import com.api.aspire.data.restapi.AspireConcierge;
import com.api.aspire.data.restapi.AspireCreateProfileApi;
import com.api.aspire.data.restapi.AspireOAuthApi;
import com.api.aspire.data.restapi.AspirePassCodeApi;
import com.api.aspire.data.restapi.AspireProfileApi;
import com.api.aspire.data.restapi.OKTAUserApi;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppHttpClient extends Retro2Client {

    private AspireConcierge aspireConcierge;
    private AspireOAuthApi aspireOAuthApi;
    private AspireProfileApi aspireProfileApi;
    private AspireCreateProfileApi aspireCreateProfileApi;
    private OKTAUserApi oktaUserApi;
    private AspirePassCodeApi aspirePassCodeApi;

    private static class AppHttpClientHelper {
        private static final AppHttpClient INSTANCE = new AppHttpClient();
    }

    public static AppHttpClient getInstance() {
        return AppHttpClientHelper.INSTANCE;
    }

    public AspireConcierge getAspireConcierge() {
        return aspireConcierge;
    }

    public void setAspireConcierge(AspireConcierge aspireConcierge) {
        this.aspireConcierge = aspireConcierge;
    }

    public AspireOAuthApi getAspireOAuthApi() {
        return aspireOAuthApi;
    }

    public void setAspireOAuthApi(AspireOAuthApi aspireOAuthApi) {
        this.aspireOAuthApi = aspireOAuthApi;
    }

    public AspireProfileApi getAspireProfileApi() {
        return aspireProfileApi;
    }

    public void setAspireProfileApi(AspireProfileApi aspireProfileApi) {
        this.aspireProfileApi = aspireProfileApi;
    }

    public AspireCreateProfileApi getAspireCreateProfileApi() {
        return aspireCreateProfileApi;
    }

    public void setAspireCreateProfileApi(AspireCreateProfileApi aspireCreateProfileApi) {
        this.aspireCreateProfileApi = aspireCreateProfileApi;
    }

    public OKTAUserApi getOktaUserApi() {
        return oktaUserApi;
    }

    public void setOktaUserApi(OKTAUserApi oktaUserApi) {
        this.oktaUserApi = oktaUserApi;
    }

    public AspirePassCodeApi getAspirePassCodeApi() {
        return aspirePassCodeApi;
    }

    public void setAspirePassCodeApi(AspirePassCodeApi aspirePassCodeApi) {
        this.aspirePassCodeApi = aspirePassCodeApi;
    }

    private AppHttpClient() {
        final Retrofit aspireCaseRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_ASPIRE_CONCIERGE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient())
                .build();

        final Retrofit aspireOauthRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_ASPIRE_TOKEN)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient())
                .build();

        final Retrofit aspireCreateProfileRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_ASPIRE_CREATE_PROFILE)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient())
                .build();

        final Retrofit aspireProfileRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_ASPIRE_PROFILE)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient())
                .build();

        final Retrofit oktaUserRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_OKTA_USER)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient())
                .build();
        final Retrofit aspirePassCodeRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_ASPIRE_PASSCODE)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient())
                .build();

        aspireOAuthApi = aspireOauthRetrofit.create(AspireOAuthApi.class);
        aspireCreateProfileApi = aspireCreateProfileRetrofit.create(AspireCreateProfileApi.class);
        aspireProfileApi = aspireProfileRetrofit.create(AspireProfileApi.class);
        aspireConcierge = aspireCaseRetrofit.create(AspireConcierge.class);
        oktaUserApi = oktaUserRetrofit.create(OKTAUserApi.class);
        aspirePassCodeApi = aspirePassCodeRetrofit.create(AspirePassCodeApi.class);

    }


}
