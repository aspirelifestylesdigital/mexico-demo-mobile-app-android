package com.api.aspire.data.entity.profile;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 6/6/18.
 */
public class UpdateProfileAspireResponse {

    @SerializedName("preferences")
    Preferences preferences;
    @SerializedName("locations")
    Locations locations;
    @SerializedName("contacts")
    Contacts contacts;
    @SerializedName("partyId")
    String partyid;

    private class Preferences {
        @SerializedName("status")
        String status;
    }

    private class Locations {
        @SerializedName("status")
        String status;
    }

    private class Contacts {
        @SerializedName("status")
        String status;
    }

    public boolean isSuccess() {
        return (!TextUtils.isEmpty(partyid)
                && preferences != null && preferences.status.equalsIgnoreCase("updated")
                && locations != null && locations.status.equalsIgnoreCase("updated")
                && contacts != null && contacts.status.equalsIgnoreCase("updated")
        );
    }
}
