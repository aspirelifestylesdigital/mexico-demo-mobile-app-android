package com.api.aspire.common.logic;

import android.text.TextUtils;
import android.util.Base64;

import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public final class EncryptData {

    private final String ALGORITHM = "AES";
    private final String KEY = "FPTLb4YpWq"; //-- random value

    private static class EncryptDataHelper {
        private static final EncryptData INSTANCE = new EncryptData();
    }

    private EncryptData() {

    }

    public static EncryptData getInstance() {
        return EncryptDataHelper.INSTANCE;
    }

    public String encrypt(String valueData){
        if(TextUtils.isEmpty(valueData)){
            return "";
        }
        try {
            SecretKeySpec key = generateKey();
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encVal = cipher.doFinal(valueData.getBytes("UTF-8"));
            return Base64.encodeToString(encVal, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String decrypt(String valueEncrypt){
        if(TextUtils.isEmpty(valueEncrypt)){
            return "";
        }
        try {
            SecretKeySpec key = generateKey();
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decryptedValue64 = Base64.decode(valueEncrypt, Base64.DEFAULT);
            byte[] decryptedByteValue = cipher.doFinal(decryptedValue64);
            return new String(decryptedByteValue, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private SecretKeySpec generateKey() throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] bytes = KEY.getBytes("UTF-8");
        digest.update(bytes, 0, bytes.length);
        byte[] key = digest.digest();
        return new SecretKeySpec(key, ALGORITHM);
    }



}
