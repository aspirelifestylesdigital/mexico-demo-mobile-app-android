package com.api.aspire.data.repository;

import android.text.TextUtils;

import com.api.aspire.data.datasource.LocalProfileDataStore;
import com.api.aspire.data.datasource.RemoteProfileDataStoreAspire;
import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.data.entity.profile.UpdateProfileAspireRequest;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;
import com.api.aspire.domain.usecases.MapProfileBase;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public class ProfileDataAspireRepository implements ProfileAspireRepository {

    private LocalProfileDataStore local;
    private RemoteProfileDataStoreAspire remote;
    private PreferencesStorageAspire pref;

    private MapProfileBase mapLogic;

    public ProfileDataAspireRepository(PreferencesStorageAspire pref, MapProfileBase mapLogic) {
        this.pref = pref;
        this.local = new LocalProfileDataStore(pref);
        this.remote = new RemoteProfileDataStoreAspire(pref);
        this.mapLogic = mapLogic;
    }

    @Override
    public Single<ProfileAspire> signInProfile(String accessToken, String password, String userNameEmail) {

        return remote.loadProfile(accessToken)
                .flatMap(profileAspire -> logicProfile(profileAspire, password, userNameEmail))
                .flatMap(profile -> local.saveProfile(profile).andThen(local.loadProfile()));
    }

    @Override
    public Single<ProfileAspire> signInNoSaveStorage(String accessToken, String password, String userNameEmail) {
        return remote.loadProfile(accessToken)
                .flatMap(profileAspire -> logicProfile(profileAspire, password, userNameEmail));
    }


    @Override
    public Single<ProfileAspire> saveProfileStorage(ProfileAspire profileAspire) {
        return local.saveProfile(profileAspire).andThen(local.loadProfile());
    }

    @Override
    public Single<ProfileAspire> loadProfile(String accessToken) {
        if (TextUtils.isEmpty(accessToken)) {
            return local.loadProfile();
        } else {
            return local.loadProfile()
                    .flatMap(prLocal ->
                            remote.loadProfile(accessToken)
                                    .flatMap(profileAspire -> logicProfile(profileAspire, prLocal.getSecretKeyDecrypt(), prLocal.getEmail()))
                    )
                    .flatMap(profile -> local.saveProfile(profile).andThen(local.loadProfile()));
        }
    }

    private Single<ProfileAspire> logicProfile(ProfileAspire profileAspire, String password, String userNameEmail) {
        return mapLogic.mapResponse(pref, profileAspire, password, userNameEmail);
    }

    /* mapLogic don't use */
    @Override
    public Completable saveProfile(String accessToken, ProfileAspire profile) {

        String passCodeLocal = (pref == null)
                ? ""
                : pref.getPassCode();

        UpdateProfileAspireRequest updateProfileAspireRequest = mapLogic.mapUpdate(profile, passCodeLocal);

        return remote.saveProfile(accessToken, updateProfileAspireRequest)
                .andThen(local.saveProfile(profile));
    }

    /* mapLogic don't use */
    @Override
    public Completable createProfile(String serviceToken,
                                     CreateProfileAspireRequest createProfileRequest,
                                     ProfileAspire profile) {

        return remote.createProfile(serviceToken, createProfileRequest)
                .andThen(local.saveProfile(profile));
    }
}
