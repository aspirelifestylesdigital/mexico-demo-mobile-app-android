package com.api.aspire.data.entity.profile;

import com.google.gson.annotations.SerializedName;

public class CreateProfileAspireRequest {

    @SerializedName("verificationMetadata")
    private VerificationMetadata verificationMetadata;

    @SerializedName("profile")
    private ProfileAspireResponse profile;

    @SerializedName("account")
    private Account account;

    public CreateProfileAspireRequest(String email, String password,
                                      ProfileAspireResponse profileAspireResponse,
                                      int bin) {
        this.profile = profileAspireResponse;
        this.account = new Account(email, password);
        //-- bin verificationMetadata
        this.verificationMetadata = new VerificationMetadata(bin);
    }

    private class Account {
        @SerializedName("email")
        private String email;
        @SerializedName("password")
        private String password;
        @SerializedName("username")
        private String username;
        @SerializedName("activate")
        private boolean activate;

        public Account(String email, String password) {
            this.email = email;
            this.password = password;
            this.username = email;
            this.activate = true;
        }
    }
}
