package com.api.aspire.domain.usecases;

import com.api.aspire.common.exception.BackendException;
import com.api.aspire.data.retro2client.AppHttpClient;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class RetrievePassword extends UseCase<Void, String> {

    @Override
    Observable<Void> buildUseCaseObservable(String s) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(String s) {
        return null;
    }

    @Override
    Completable buildUseCaseCompletable(String email) {
        return Completable.create(e -> {

            Call<ResponseBody> request = AppHttpClient.getInstance().getOktaUserApi().forgotPassword(email);
            Response<ResponseBody> response = request.execute();

            if (response.isSuccessful() && response.code() == 200) {
                e.onComplete();
            } else {
                if (response.code() == 404) {
                    e.onError(new BackendException(response.errorBody().string()));
                } else {
                    e.onError(new Exception(response.errorBody().string()));
                }
            }
        });
    }
}